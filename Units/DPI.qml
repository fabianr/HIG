import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

Rectangle {
    width: 800
    height: 400
    color: "white"

    Row {
        Item {
            id: desktop
            width: 400
            height: 400
            Image {
                source: "../img/video-display.svg"
                anchors.fill: parent;
                sourceSize.width: parent.width
                sourceSize.height: parent.height
            }

            Rectangle {
                x: 50
                y: 62
                width: 301
                height: 224

                // Draw helpers and anotation
                BaselineGrid {
                    z: 1
                    base: units.gridUnit / 2
                    color: "rgba(0, 0, 0, 0.2)"
                    label: ""

                }

                Rectangle {
                    width: units.largeSpacing * 2
                    height: units.largeSpacing * 2
                    x: units.largeSpacing * 7
                    y: units.largeSpacing * 3
                    color: "#da4453"
                }

                Rectangle {
                    width: units.largeSpacing * 2
                    height: units.largeSpacing * 2
                    x: units.largeSpacing * 7
                    y: units.largeSpacing * 8
                    color: "#27ae60"
                }
            }

        }

        Item {
            id: mobile
            width: 400
            height: 400
            Image {
                source: "../img/smartphone.svg"
                anchors.fill: parent;
                sourceSize.width: parent.width
                sourceSize.height: parent.height
            }

            Rectangle {
                x: 120
                y: 70
                width: 160
                height: 260

                // Draw helpers and anotation
                BaselineGrid {
                    z: 1
                    base: units.gridUnit / 4
                    color: "rgba(0, 0, 0, 0.2)"
                    label: ""
                }

                Rectangle {
                    width: units.largeSpacing
                    height: units.largeSpacing
                    x: units.largeSpacing * 3
                    y: units.largeSpacing * 3
                    color: "#da4453"
                }

                Rectangle {
                    width: units.largeSpacing * 2
                    height: units.largeSpacing * 2
                    x: units.largeSpacing * 3
                    y: units.largeSpacing * 8
                    color: "#27ae60"
                }
            }



        }
     }
}
