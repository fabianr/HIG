import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

Rectangle {
    width: units.largeSpacing * 8
    height: units.largeSpacing * 6
    color: "white"
    scale: 2
    id: root
    transformOrigin: Item.TopLeft

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: units.gridUnit
        color: "rgba(0, 0, 0, 0.1)"
    }

    Item {
        x: units.largeSpacing
        y: units.largeSpacing

        Item {
            Text {
                id: txt
                text: "Mg"
                font.family: "Noto"
            }
        }

    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            var brace = Qt.createComponent("../Brace.qml");
            var outline = Qt.createComponent("../Outline.qml");
            var ruler = Qt.createComponent("../Ruler.qml");
            ruler.createObject(root, {ry: txt.mapToItem(root, 0, 0).y});
            ruler.createObject(root, {ry: txt.mapToItem(root, 0, 0).y + txt.height});
        }
    }
}
