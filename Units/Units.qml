import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

Rectangle {
    width: 800
    height: 400
    color: "white"

    Row {
        Item {
            id: desktop
            width: 400
            height: 400
            Image {
                source: "../img/video-display.svg"
                anchors.fill: parent;
                sourceSize.width: parent.width
                sourceSize.height: parent.height
            }

            Rectangle {
                x: 50
                y: 62
                width: 301
                height: 224

                // Draw helpers and anotation
                BaselineGrid {
                    z: 1
                    base: units.gridUnit / 2
                    color: "rgba(0, 0, 0, 0.1)"
                    label: ""
                }

                Rectangle {
                    width: units.largeSpacing * 5
                    height: units.largeSpacing
                    x: units.largeSpacing * 5
                    y: units.largeSpacing * 3
                    color: "#3daee9"

                    Text {
                        x: units.smallSpacing
                        text: "Font size 10"
                        font.pointSize: 10
                        color: "white"
                    }
                }

                Rectangle {
                    width: units.largeSpacing * 5 * 1.5
                    height: units.largeSpacing * 1.5
                    x: units.largeSpacing * 5
                    y: units.largeSpacing * 6
                    color: "#3daee9"

                    Text {
                        x: units.smallSpacing  * 1.5
                        text: "Font size 15"
                        font.pointSize: 15
                        color: "white"
                    }
                }
            }

        }

        Item {
            id: mobile
            width: 400
            height: 400
            Image {
                source: "../img/smartphone.svg"
                anchors.fill: parent;
                sourceSize.width: parent.width
                sourceSize.height: parent.height
            }

            Rectangle {
                x: 120
                y: 70
                width: 160
                height: 260

                // Draw helpers and anotation
                BaselineGrid {
                    z: 1
                    base: units.gridUnit / 4
                    color: "rgba(0, 0, 0, 0.1)"
                    label: ""
                }

                Rectangle {
                    width: units.largeSpacing * 5
                    height: units.largeSpacing
                    x: units.largeSpacing
                    y: units.largeSpacing * 3
                    color: "#3daee9"

                    Text {
                        x: units.smallSpacing
                        text: "Font size 10"
                        font.pointSize: 10
                        color: "white"
                    }
                }

                Rectangle {
                    width: units.largeSpacing * 5 * 1.5
                    height: units.largeSpacing * 1.5
                    x: units.largeSpacing
                    y: units.largeSpacing * 6
                    color: "#3daee9"

                    Text {
                        x: units.smallSpacing  * 1.5
                        text: "Font size 15"
                        font.pointSize: 15
                        color: "white"
                    }
                }
            }
        }
     }
}
