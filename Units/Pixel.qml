import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

Rectangle {
    width: 800
    height: 400
    color: "white"

    Row {
        Item {
            id: desktop
            width: 400
            height: 400
            Image {
                source: "../img/video-display.svg"
                anchors.fill: parent;
                sourceSize.width: parent.width
                sourceSize.height: parent.height
            }

            Rectangle {
                x: 50
                y: 62
                width: 301
                height: 224

                // Draw helpers and anotation
                BaselineGrid {
                    z: 1
                    base: units.gridUnit / 2
                    color: "rgba(0, 0, 0, 0.1)"
                    label: ""
                }

                Rectangle {
                    width: 30
                    x: units.largeSpacing
                    y: parent.height - units.largeSpacing
                    height: 2
                    color: "#ECA1A9"
                }
                Text {
                    color: "#ECA1A9"
                    font.pointSize: 12
                    lineHeight: 12
                    height: 12
                    text: "1 inch"
                    x: units.largeSpacing
                    y: parent.height - units.largeSpacing - 20
                }
            }

        }

        Item {
            id: mobile
            width: 400
            height: 400
            Image {
                source: "../img/smartphone.svg"
                anchors.fill: parent;
                sourceSize.width: parent.width
                sourceSize.height: parent.height
            }

            Rectangle {
                x: 120
                y: 70
                width: 160
                height: 260

                // Draw helpers and anotation
                BaselineGrid {
                    z: 1
                    base: units.gridUnit / 4
                    color: "rgba(0, 0, 0, 0.1)"
                    label: ""
                }

                Rectangle {
                    width: 30
                    x: units.largeSpacing
                    y: parent.height - units.largeSpacing
                    height: 2
                    color: "#ECA1A9"
                }
                Text {
                    color: "#ECA1A9"
                    font.pointSize: 12
                    lineHeight: 12
                    height: 12
                    text: "1 inch"
                    x: units.largeSpacing
                    y: parent.height - units.largeSpacing - 20
                }
            }



        }
     }
}
