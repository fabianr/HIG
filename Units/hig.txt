__NOTOC__
== Purpose ==
This pages gives an overview about different units used in KDE and Plasma

== Pixel ==
A (physical) pixel or dot is a physical point in a raster image, or the smallest addressable element in an all points addressable display device. <span style="color: red">Be careful not to confuse it with DPI independent pixels</span>

== DPI - pixels per inch ==
Pixel density is the number of (physical) pixels or dots that fit into an inch. Different screens have different DPI.
<br/><br/>
<b>screen density = screen width (or height) in pixels / screen width (or height) in inches</b>
<br/><br/>

{|
|[[Image:Pixel.qml.png|Different DPIs on desktop and mobile]]
|}

DPI is often used interchangeably with PPI, pixel per inch.

== PPI / DPI independent pixels ==
A DPI independet pixel is scaled to look uniform on any screen regardless of the DPI. A lot of platforms, eg iOS, Android, the Web, replaced the old (physical) px with a DPI px. So most the time you read about pixel/px they actually talk about DPI independent pixels. Qt (and QML) support DPI independent pixels in newer versions, but because KDE and Plasma support older versions of Qt too, one can not assume that pixels used in Qt or QML apps are DPI independent.

{|
|[[Image:Dpi.qml.png|Different DPIs on desktop and mobile]]
|}
A rectangle defined with <span style="color: #da4453">physical pixels</span> and <span style="color: #27ae60">DPI independent pixels</span>.

<br/><br/>
<span style="color: red">Except explicilty stated otherwise, all HIG pages, draft, mockups, ... pixels/px are always DPI independent pixels.</span>
<br/><br/>

=== DPI independent pixels in KDE ===
As a developer, if you want to use DPI independent pixels use units.devicePixelRatio as a multiplier on physical pixels. Since units.devicePixelRatio is a float, make sure to round the results. Most of the time you want to floor it.

=== Fonts ===
Since KDE allows the user to change the font settings any dimensions defined with px, no matter if they are DPI independent or not, make problems together with text.

{|
|[[Image:Font.qml.png|Using DPI independet pixel with different font setting]]
|}

== base units in plasma ==
There are special base units in plasma:

* units.smallSpacing, for mockup and design = 4px
* units.largeSpacing, for mockup and design = 18px
* units.gridUnit, for mockup and design = 18px

These are not only DPI independent but scale according to the font settings too. While designing, be careful not to rely on the ratio between units.smallSpacing and units.largeSpacing because these change depending on font settings.
 
{|
|[[Image:Units.qml.png|Using units.smallSpacing with different font setting]]
|}
A rectangle defined with <span style="color: #3daee9">units.smallSpacing</span>

== From design to code ==
=== recomended spacings ===
If you design try to use the recomended values for margin and paddings, to ensure a uniform appearance. See [[../Placement|Placement and Spacing]] for more details.
{|
|[[Image:Margin.qml.png|Use of base units]]
|}

<syntaxhighlight lang="qml" line>
Row {
	spacing: units.largeSpacing
	Rectangle {
		...
	}
	Rectangle {
		...
	}
}
</syntaxhighlight>

<syntaxhighlight lang="qml" line>
Row {
	spacing: 2 * units.smallSpacing
	Rectangle {
		...
	}
	Rectangle {
		...
	}
}
</syntaxhighlight>

=== arbitrary px values ===
When needed, you can use arbitrary px values for your mockups. As a developer you need to use units.devicePixelRatio to make these values DPI independent.
{|
|[[Image:Arbitrary.qml.png|Use of arbitrary px values]]
|}
<syntaxhighlight lang="qml" line>
Row {
	spacing: units.smallSpacing
	Rectangle {
		height: units.largeSpacing
		width: Math.floor(2 * units.devicePixelRatio)
	}
	Text {
		...
	}
}
</syntaxhighlight>

=== ratio ===
Sometimes the ratio between dimensions is more important then the actually values.

{|
|[[Image:Ratio.qml.png|]]
|}
<syntaxhighlight lang="qml" line>
Grid {
	columns: 3
	...
	Repeater {
		model: 9
		...
		Rectangle {
			width: grid.width / 3
			height: grid.height / 3
			...
		}
	}
}
</syntaxhighlight>
