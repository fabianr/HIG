import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

Rectangle {
    width: units.largeSpacing * 13
    height: units.largeSpacing * 13
    color: "white"
    id: root

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: units.gridUnit
        color: "rgba(0, 0, 0, 0.2)"
    }

    Rectangle {
        x: units.largeSpacing * 2
        y: units.largeSpacing

        width: units.largeSpacing * 10
        height : units.largeSpacing * 8

        Grid {
            columns: 3
            anchors.fill: parent;
            id: grid

            Repeater {
                model: 9
                id: rep
                Rectangle {
                    width: grid.width / 3
                    height: grid.height / 3
                    border.width: 1
                    border.color: "#31363b"
                }
            }
        }
    }

    Text {
        x: units.largeSpacing + grid.width / 6 - units.smallSpacing
        y: 2
        color: "#ECA1A9"
        font.pointSize: 10
        lineHeight: 10
        height: 10
        text: "1/3"
    }

    Text {
        x: units.largeSpacing + grid.width / 2 - units.smallSpacing
        y: 2
        color: "#ECA1A9"
        font.pointSize: 10
        lineHeight: 10
        height: 10
        text: "1/3"
    }

    Text {
        x: units.largeSpacing + grid.width / 6 * 5 - units.smallSpacing
        y: 2
        color: "#ECA1A9"
        font.pointSize: 10
        lineHeight: 10
        height: 10
        text: "1/3"
    }

    Text {
        y: units.largeSpacing + grid.height / 6 - units.smallSpacing
        x: 2
        color: "#ECA1A9"
        font.pointSize: 10
        lineHeight: 10
        height: 10
        text: "1/3"
    }

    Text {
        y: units.largeSpacing + grid.height / 2 - units.smallSpacing
        x: 2
        color: "#ECA1A9"
        font.pointSize: 10
        lineHeight: 10
        height: 10
        text: "1/3"
    }

    Text {
        y: units.largeSpacing + grid.height / 6 * 5 - units.smallSpacing
        x: 2
        color: "#ECA1A9"
        font.pointSize: 10
        lineHeight: 10
        height: 10
        text: "1/3"
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            var brace = Qt.createComponent("../Brace.qml");
            var outline = Qt.createComponent("../Outline.qml");
            var ruler = Qt.createComponent("../Ruler.qml");
        }
    }
}
