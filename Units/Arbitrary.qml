import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents


Rectangle {
    width: units.largeSpacing * 20
    height: units.largeSpacing * 12

    Rectangle {
        width: units.largeSpacing * 10
        height: units.largeSpacing * 6
        color: "white"
        id: root
        scale: 2
        transformOrigin: Item.TopLeft

        // Draw helpers and anotation
        BaselineGrid {
            z: 1
            base: units.gridUnit
            color: "rgba(0, 0, 0, 0.1)"
        }

        Item {
            anchors.fill: parent
            Row {
                spacing: units.smallSpacing
                x:  units.largeSpacing
                y:  units.largeSpacing

                Rectangle {
                    id: bar
                    width: Math.floor(2 * units.devicePixelRatio)
                    height: units.largeSpacing
                    color: "#f67400"
                }
                Text {
                    id: txt
                    text: "Hello world"
                }
            }
        }

        Text {
            text: "width: 2px"
            color: "#ECA1A9"
            font.pixelSize: 8
            x:  units.largeSpacing
            y:  2 * units.largeSpacing + units.smallSpacing
        }

        Text {
            anchors.top: parent.top;
            anchors.left: parent.left;
            anchors.margins: units.smallSpacing
            text: "zoom 200%"
            color: "#7f8c8d"
            font.pixelSize: 5
        }

        // HACK coordinates are only final after a small delay
        Timer {
            interval: 1000
            repeat: false
            running: true
            onTriggered: {
                var brace = Qt.createComponent("../Brace.qml");
                var outline = Qt.createComponent("../Outline.qml");
                var ruler = Qt.createComponent("../Ruler.qml");
                var messure = Qt.createComponent("../Messure.qml");
                //brace.createObject(root, {"from": bar, "to": txt, "text": "4"});
                //outline.createObject(root, {item: txt, label: false});
                //ruler.createObject(root, {ry: txt.mapToItem(root, 0, 0).y});
                //ruler.createObject(root, {ry: txt.mapToItem(root, 0, 0).y + txt.height});
            }
        }
    }
}
