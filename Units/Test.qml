import QtQuick 2.7
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.kirigami 2.0 as Kirigami

Rectangle {
    width: 400
    height: 400
    color: "white"

    Component.onCompleted: function() {
        console.log("plasma")
        for (var size in units.iconSizes) {
            if (typeof units.iconSizes[size] !== "function")
                console.log(size + ": " + units.iconSizes[size])
        }
        console.log("kirigami")
        for (size in Kirigami.Units.iconSizes) {
            if (typeof Kirigami.Units.iconSizes[size] !== "function")
                console.log(size + ": " + Kirigami.Units.iconSizes[size])
        }
    }
}
