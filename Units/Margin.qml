import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

Rectangle {
    width: units.largeSpacing * 9
    height: units.largeSpacing * 12
    color: "white"

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: units.gridUnit
        color: "rgba(0, 0, 0, 0.1)"
    }

    Item {
        anchors.fill: parent
        id: root

        Row {
            spacing: units.largeSpacing
            x: units.largeSpacing * 2
            y: units.largeSpacing * 2

            Rectangle {
                id: left1
                width: units.largeSpacing * 2
                height: units.largeSpacing * 2
                color: "#3daee9"
            }

            Rectangle {
                id: right1
                width: units.largeSpacing * 2
                height: units.largeSpacing * 2
                color: "#3daee9"
                anchors.top: parent.top;
            }
        }

        Row {
            spacing: units.smallSpacing * 2
            x: units.largeSpacing * 2
            y: units.largeSpacing * 6

            Rectangle {
                id: left2
                width: units.largeSpacing * 2
                height: units.largeSpacing * 2
                color: "#3daee9"
            }

            Rectangle {
                id: right2
                width: units.largeSpacing * 2
                height: units.largeSpacing * 2
                color: "#3daee9"
                anchors.top: parent.top;
            }
        }

    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            var brace = Qt.createComponent("../Brace.qml");
            brace.createObject(root, {"from": left1, "to": right1, "text": "18px", "center": true});
            brace.createObject(root, {"from": left2, "to": right2, "text": "8px = 2 * 4px", "center": true});
        }
    }
}
