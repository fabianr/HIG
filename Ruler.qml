//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

// Draw a ruler for highlighting alignment
Item {
    id: canvas
    anchors.fill: parent;
    property int rx;
    property int ry;
    property bool horizontal: true;
    property string stroke: "rgba(41,128,185, 1)"

    // using Rectabgles because they scale smooth
    Row {
        y: canvas.ry
        x: 0
        id: hackRow
        visible: canvas.horizontal
        spacing: units.smallSpacing / csale()
        Repeater {
           model: new Array(Math.floor(canvas.width / hackRow.spacing / 2))
           Rectangle {
               width: units.smallSpacing / csale()
               height: 2 / csale()
               color: "#2980b9"
           }
        }
        Component.onCompleted: {
            //console.log(canvas.ry)
        }
    }
    Column {
        x: canvas.rx
        y: 0
        id: hackColumn
        visible: !canvas.horizontal
        spacing: units.smallSpacing / csale()
        Repeater {
           model: new Array(Math.floor(canvas.height / hackColumn.spacing / 2))
           Rectangle {
               height: units.smallSpacing / csale()
               width: 2 / csale()
               color: "#2980b9"
           }
        }
        Component.onCompleted: {
            //console.log(canvas.rx)
        }
    }

    function csale() {
        var scale = 1
        var n = canvas
        while (n !== null) {
            scale = scale * n.scale
            n = n.parent
        }
        return scale;
    }

    /* does not sclae smooth
    Canvas {
        anchors.fill: parent;

        onPaint: {

            // get scale because annotation should not be scaled
            var n = canvas.parent
            var scale = 1
            while (n !== null) {
                scale = scale * n.scale
                n = n.parent
            }

            var ctx = getContext("2d");
            ctx.strokeStyle = canvas.stroke;
            ctx.lineWidth = 1 / scale
            ctx.beginPath();
            // Draw the Ruler
            if (horizontal) {
                var xt = 0;
                while (xt < canvas.width) {
                    ctx.moveTo(xt, ry - ctx.lineWidth);
                    xt += units.smallSpacing;
                    ctx.lineTo(xt, ry - ctx.lineWidth);
                    xt += units.smallSpacing;
                }
            }
            else {
                var yt = 0;
                while (yt < canvas.height) {
                    ctx.moveTo(rx, yt);
                    yt += units.smallSpacing;
                    ctx.lineTo(rx, yt);
                    yt += units.smallSpacing;
                }
            }
            ctx.stroke();
         }
    }*/
}
