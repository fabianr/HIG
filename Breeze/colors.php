<?php
global $names;
$names = [
	"#fcfcfc" => "Paper White",
	"#eff0f1" => "Cardboard Grey",
	"#4d4d4d" => "Icon Grey",
	"#31363b" => "Charcoal Grey",
	"#232629" => "Shade Black",
	"#3daee9" => "Plasma Blue",
	"#da4453" => "Icon Red",
	"#ed1515" => "Danger Red",
	"#f47750" => "Icon Orange",
	"#f67400" => "Beware Orange",
	"#fdbc4b" => "Icon Yellow",
	"#c9ce3b" => "Sunbeam Yellow",
	"#1cdc9a" => "Mellow Turquoise",
	"#2ecc71" => "Icon Green",
	"#11d116" => "Verdant Green",
	"#1d99f3" => "Icon Blue",
	"#e74c3c" => "Pimpinella",
	"#c0392b" => "Quartz",
	"#d35400" => "Ocher",
	"#f39c1f" => "Sundown",
	"#27ae60" => "Noble Fir",
	"#1abc9c" => "Turquoise",
	"#16a085" => "Dark Turquoise",
	"#3498db" => "Ocean Blue",
	"#2980b9" => "Abyss Blue",
	"#9b59b6" => "Mallow Pink",
	"#8e44ad" => "Purple Haze",
	"#34495e" => "Night Blue",
	"#2c3e50" => "Late-night Blue",
	"#7f8c8d" => "Coastal Fog",
	"#95a5a6" => "Winter Smog",
	"#bdc3c7" => "Alternate Grey",
	"#1e92ff" => "Deco Blue",
	"#93cee9" => "Hover Blue",
	"#3b4045" => "Burnt Charcoal",
	"#afb0b3" => "Lazy Grey",
	"#3daee6" => "Hyper Blue"
];

exec("rm -r ./gen; mkdir ./gen");
create("", "Breeze.colors");
create("-dark", "BreezeDark.colors");
create("-light", "BreezeLight.colors");
create("-contrast", "BreezeHighContrast.colors");


function create($type, $filename) {
	global $colors;
	$colors = parse_ini_file("../../Breeze/colors/" . $filename, true);
	createHIG($type);
	createSCSS($type);
	createGIMP($type);
}
	
//print_r($colors);
// color names


function createSCSS($type) {
	global $colors;
	$str = "";
	foreach ($colors as $section => $content) {
		if (stristr($section, "Colors:") !== FALSE) {
			$section = lcfirst(substr($section, 7));
			foreach ($content as $name => $color) {
				$str .= '$' . $section . $name . ": rgb(" . $color . ");\n";
			}
		}
	}
	$filename = "gen/breeze-colors" . $type . ".scss";
	file_put_contents($filename, $str);
	exec("gzip -k " . $filename);
}

function createHIG($type) {
	global $colors;
	global $names;
	$str = "<table style='width: 100%!important;' border='1px'>\n";
	$str .= "<tr><th>Color Set</th><th>Color Role</th><th>Color</th><th>hex</th><th>RGB</th><th>Name</th></tr>\n";
	foreach ($colors as $section => $content) {
		if (stristr($section, "Colors:") !== FALSE) {
			$section = lcfirst(substr($section, 7));
			$str .= "<tr><td rowspan='" . count($content) ."'>" . $section . "</td>";
			$first = true;
			foreach ($content as $name => $color) {
				if (!$first) {
					$str .= "<tr>";
				}
				$first = false;
				$str .= "<td>" . $name . "</td><td style='background-color: rgb(" . $color .");'>&#160;</td><td>" . rgb2hex($color) . "</td><td>" . $color . "</td><td>";
				if (isset($names[rgb2hex($color)])) {
					$str .= $names[rgb2hex($color)];
				}
				$str .= "</td></tr>\n";
			}
		}
	}
	$str .= "</table>";
	$filename = "gen/breeze-colors" . $type . ".html";
	file_put_contents($filename, $str);
	exec("gzip -k " . $filename);
}

function createGIMP($type) {
	global $colors;
	$str = "GIMP Palette\n";
	$str .= "Name: Breeze\n#\n";
	foreach ($colors as $section => $content) {
		if (stristr($section, "Colors:") !== FALSE) {
			$section = lcfirst(substr($section, 7));
			foreach ($content as $name => $color) {
				$rgb = explode(",", $color);
				$str .= str_pad($rgb[0], 3, " ", STR_PAD_LEFT) . " " . str_pad($rgb[1], 3, " ", STR_PAD_LEFT) . " " . str_pad($rgb[2], 3, " ", STR_PAD_LEFT) . "\t" . $name . "\n";
			}
		}
	}
	$filename = "gen/breeze-colors" . $type . ".gpl";
	file_put_contents($filename, $str);
	exec("gzip -k " . $filename);
}


function rgb2hex($rgb) {
	if (!is_array($rgb)) {
		$rgb = explode(",", $rgb);
	}
	$hex = "#";
	$hex .= str_pad(dechex($rgb[0]), 2, "0", STR_PAD_LEFT);
	$hex .= str_pad(dechex($rgb[1]), 2, "0", STR_PAD_LEFT);
	$hex .= str_pad(dechex($rgb[2]), 2, "0", STR_PAD_LEFT);

	return $hex; // returns the hex value including the number sign (#)
}