//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

//window containing the application
Item {
    anchors.fill: parent;
    property string color: "rgba(236, 161, 169, 0.6)"
    property bool label: false
    property Item item
    property Item root : container.parent
    z: 10
    id: container

    Rectangle {
        id: prot
        color: "#DDcccccc"
        width: childrenRect.width + 10
        height: childrenRect.height + 10
        visible: false
        z: 2

        Text {
            x: 4
            id: dim
            color: "#2980b9"
            font.pointSize: 8
            lineHeight: 8
            height: 8

        }
    }

    Canvas {
        anchors.fill: parent;
        id: canvas

        onPaint: {
            // get scale because annotation should not be scaled
            var n = container.parent
            var scale = 1
            while (n !== null) {
                scale = scale * n.scale
                n = n.parent
            }

            var ctx = getContext("2d");
            ctx.strokeStyle = container.color;
            ctx.lineWidth = 4 / scale
            ctx.beginPath();

            // Horizontal grid lines
            var offset = ctx.lineWidth / 2;
            //console.log(item.toString() + "  " + item.height);
            var cItem = item.mapToItem(container.root, 0, 0);
            //console.log(cItem);
            ctx.moveTo(cItem.x + offset,  cItem.y + offset);
            ctx.lineTo(cItem.x + offset, cItem.y + item.height - offset);
            ctx.lineTo(cItem.x + item.width - offset, cItem.y + item.height - offset);
            //console.log(cItem.y + item.height);
            ctx.lineTo(cItem.x + item.width - offset, cItem.y + offset);
            ctx.lineTo(cItem.x + offset, cItem.y + offset);
            ctx.stroke();

            // Write dimensions inside
            if (container.label) {
                dim.text = Math.round(item.width * 100) / 100 + " x " + Math.round(item.height * 100) / 100;
                prot.visible = true;
                prot.x = cItem.x + item.width / 2 - prot.width / 2
                prot.y = cItem.y + item.height / 2 - prot.height / 2
            }
        }
    }
}
