//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

//window containing the application
Item {
    id: canvas
    anchors.fill: parent;
    property Item from
    property Item to
    property bool flip: false
    property bool center: true
    property string text
    property string color: "rgba(236, 161, 169, 0.8)"
    property bool horizontal: true;

    Text {
        id: label
        text: canvas.text
        color: "#ECA1A9"
        font.pointSize: 8
        lineHeight: 8
        height: 8
    }


    Canvas {
        anchors.fill: parent;
        onPaint: {
            // get scale because annotation should not be scaled
            var n = canvas.parent
            var scale = 1
            while (n !== null) {
                scale = scale * n.scale
                n = n.parent
            }

            var ctx = getContext("2d");
            ctx.strokeStyle = canvas.color;
            ctx.beginPath();

            var cfrom = from.mapToItem(canvas.parent, 0, 0);
            var cto = to.mapToItem(canvas.parent, 0, 0);
            //console.log("brace: " + cfrom.x + "," + cfrom.y + " " + cto.x + "," + cto.y)

            // Determine direction
            if (horizontal) {
                //console.log("x")
                // Calculate anchor points for braces
                if (!canvas.center) {
                    // Draw from closest borders
                    if (cfrom.x > cto.x) {
                        cfrom.x = cfrom.x + 2 * units.smallSpacing
                        cto.x = cto.x + to.width - 2 * units.smallSpacing
                    }
                    else {
                        cfrom.x = cfrom.x + from.width - 2 *  units.smallSpacing
                        cto.x = cto.x + 2 * units.smallSpacing
                    }
                }
                else {
                    cfrom.x = cfrom.x + from.width / 2;
                    cto.x = cto.x + to.width / 2;
                }

                // Draw the brace
                ctx.moveTo(cfrom.x, cfrom.y);
                ctx.lineTo(cfrom.x, cfrom.y - units.smallSpacing);
                ctx.lineTo(cto.x, cfrom.y - units.smallSpacing);
                ctx.lineTo(cto.x, cfrom.y);

                // Position label
                label.x = (cfrom.x + cto.x) / 2 - label.width / 2
                label.y = cfrom.y - label.height - 2 * units.smallSpacing - 4

            }
            else {
                if (!canvas.center) {
                    // Draw from closest borders
                    if (cfrom.y > cto.y) {
                        cfrom.y = cfrom.y + 2 * units.smallSpacing
                        cto.y = cto.y + to.height - 2 * units.smallSpacing
                    }
                    else {
                        cfrom.y = cfrom.y + from.height - 2 * units.smallSpacing
                        cto.y = cto.y + 2 * units.smallSpacing
                    }
                }
                else {
                    cfrom.y = cfrom.y + from.height / 2;
                    cto.y = cto.y + to.height / 2;
                }

                // Draw the brace
                ctx.moveTo(cfrom.x, cfrom.y);
                ctx.lineTo(cfrom.x - units.smallSpacing, cfrom.y);
                ctx.lineTo(cfrom.x - units.smallSpacing, cto.y);
                ctx.lineTo(cfrom.x, cto.y);

                // Position label
                label.x = cfrom.x - label.width - 2 * units.smallSpacing
                label.y = (cfrom.y + cto.y ) / 2 - label.height / 2 - 4
            }

            ctx.stroke();
         }
    }
}
