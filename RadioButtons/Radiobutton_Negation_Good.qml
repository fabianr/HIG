//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 160
    height: 160

    GroupBox {
        title: "Settings:"
        x: 10
        y: 10
        flat: true
        style: Styles.GroupBoxStyle {}

        Column {
            spacing: 0
            CheckBox {
                text: "Apply settings"
                checked: true
            }
        }
    }
}
