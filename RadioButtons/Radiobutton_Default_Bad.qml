//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 160
    height: 160

    GroupBox {
        title: "Direction:"
        x: 10
        y: 10
        flat: true
        style: Styles.GroupBoxStyle {}

        ExclusiveGroup {
            id: radioInputGroup
        }
        Column {
            spacing: 0
            RadioButton {
                exclusiveGroup: radioInputGroup
                text: "left"
            }
            RadioButton {
                exclusiveGroup: radioInputGroup
                text: "right"
            }
        }
    }
}
