//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 160
    height: 240

    GroupBox {
        title: "Language:"
        x: 10
        y: 10
        flat: true
        style: Styles.GroupBoxStyle {}

        ExclusiveGroup {
            id: radioInputGroup
        }
        Column {
            spacing: 0
            RadioButton {
                exclusiveGroup: radioInputGroup
                text: "English"
                checked: true
            }
            RadioButton {
                exclusiveGroup: radioInputGroup
                text: "German"
            }
            RadioButton {
                exclusiveGroup: radioInputGroup
                text: "French"
            }
            RadioButton {
                exclusiveGroup: radioInputGroup
                text: "Italian"
            }
            RadioButton {
                exclusiveGroup: radioInputGroup
                text: "Spanish"
            }
            RadioButton {
                exclusiveGroup: radioInputGroup
                text: "Hebrew"
            }
        }
    }
}
