//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 240
    height: 240

    GroupBox {
        title: "Connect via:"
        x: 10
        y: 10
        flat: true
        style: Styles.GroupBoxStyle {}

        ComboBox {
            id: cbx
            model: [ "Bluetooth"]
            focus: true
        }
    }
}
