//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import QtQuick.Controls.Private 1.0

Rectangle {
    width: 240
    height: 240

    GroupBox {
        title: "Language:"
        x: 10
        y: 10
        flat: true
        style: Styles.GroupBoxStyle {}

        ExclusiveGroup {
            id: radioInputGroup
        }
        ComboBox {
            id: cbx
            model: [ "English", "German", "French", "Italian", "Spanish", "Hebrew"]
        }
    }

    // TODO when taken an automatic screenshot this doesnt work
    // HACK
    // __popup is internal and might change in future versions
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            cbx.__popup.toggleShow()
        }
    }
}
