//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import "Wizard-1.js" as Mark

Rectangle {
    id: wizard
    width: parent.width
    height: 64
    property ListModel steps;
    property int cur: 0;
    property string title;
    property string subtitle;
    property string overview;
    property int diameter: 20;
    property bool horizontal: true
    color: '#fcfcfc'

    Item {
        id: title
        state: wizard.horizontal ? "horizontal" : "vertical"

        Image {
            id: titleImg
            visible: steps.get(cur).src
            source: steps.get(cur).src
            width: diameter * 2
            height: diameter * 2
        }

        Label {
            id: titleLabel
            text: steps.get(cur).name
            font.pixelSize: 20
            color: theme.textColor
        }
        states: [
            State {
                name: "horizontal"
                PropertyChanges {
                    target: title;
                    height: diameter * 2
                    anchors.leftMargin: 16
                }
                AnchorChanges {
                    target: title;
                    anchors.left: wizard.left
                    anchors.verticalCenter: wizard.verticalCenter;
                }
                AnchorChanges {
                    target: titleImg;
                    anchors.verticalCenter: title.verticalCenter;
                }
                PropertyChanges {
                    target: titleLabel;
                    anchors.leftMargin: 16
                }
                AnchorChanges {
                    target: titleLabel;
                    anchors.verticalCenter: title.verticalCenter;
                    anchors.left: titleImg.right
                }
            },
            State {
                name: "vertical"
                PropertyChanges {
                    target: title;
                    width: wizard.width
                    anchors.topMargin: 16
                    anchors.leftMargin: 16
                }
                AnchorChanges {
                    target: title;
                    anchors.top: wizard.top
                    anchors.left: wizard.left
                }
                AnchorChanges {
                    target: titleImg;
                    //anchors.horizontalCenter: title.horizontalCenter;
                }
                PropertyChanges {
                    target: titleLabel;
                    anchors.leftMargin: 16
                }
                AnchorChanges {
                    target: titleLabel;
                    anchors.left: titleImg.right;
                    anchors.verticalCenter: titleImg.verticalCenter;
                }
            }
        ]
    }

    Component {
        id: stepComponent

        Item {
            id: stepContainer
            property alias bu: bullet
            property alias ba: bar
            property alias la: label
            state: stepsView.orientation == Qt.Horizontal ? "horizontal" : "vertical"

            Rectangle {
                id: bullet
                color: wizard.cur >= index ? theme.highlightColor : "#bdc3c7"
                radius: diameter / 2;
                width: diameter
                height: diameter

                /*Label {
                    anchors.centerIn: parent
                    text: index + 1
                    color: "white"

                }*/
            }
            Rectangle {
                id: bar
                color: cur >= index? theme.highlightColor : "#bdc3c7"
                visible: index != 0
            }
            Label {
                id: label
                text: name

                color: "#bdc3c7"
                font.pixelSize: 10
                maximumLineCount: 2;
                width: 64
                wrapMode: "WrapAtWordBoundaryOrAnywhere"
            }

            states: [
                State {
                    name: "horizontal"
                    PropertyChanges {
                        target: stepContainer;
                        y: 16
                        width: index != 0 ? (80) : diameter
                        height: 64 - 16
                    }
                    AnchorChanges {
                        target: bullet;
                        anchors.right: parent.right
                    }
                    PropertyChanges {
                        target: bar;
                        width: 64;
                        height: 4;
                        anchors.rightMargin: -4
                    }
                    AnchorChanges {
                        target: bar;
                        anchors.verticalCenter: bullet.verticalCenter;
                        anchors.right: bullet.left;

                    }
                    PropertyChanges {
                        target: label
                        horizontalAlignment: Text.AlignHCenter
                    }
                    AnchorChanges {
                        target: label;
                        anchors.horizontalCenter: bullet.horizontalCenter
                        anchors.top: bullet.bottom
                    }
                },
                State {
                    name: "vertical"
                    PropertyChanges {
                        target: stepContainer;
                        x: 16
                        height: index != 0 ? (80) : diameter
                        width:  64 - 16
                    }
                    AnchorChanges {
                        target: bullet;
                        anchors.bottom: parent.bottom
                    }
                    PropertyChanges {
                        target: bar;
                        width: 4
                        height: 64
                        anchors.bottomMargin: -4
                    }
                    AnchorChanges {
                        target: bar;
                        anchors.horizontalCenter: bullet.horizontalCenter;
                        anchors.bottom: bullet.top;
                    }
                    PropertyChanges {
                        target: label
                        verticalAlignment: Text.Left
                    }
                    PropertyChanges {
                        target: label;
                        anchors.leftMargin: 8
                    }
                    AnchorChanges {
                        target: label;
                        anchors.verticalCenter: bullet.verticalCenter;
                        anchors.left: bullet.right
                    }
                }
            ]
        }
    }

    ListView {
        id: stepsView
        model: wizard.steps
        delegate: stepComponent
        currentIndex: cur
        state: wizard.horizontal ? "horizontal" : "vertical"

        states: [
            State {
                name: "horizontal"
                PropertyChanges {
                    target: stepsView;
                    anchors.rightMargin: 16
                    orientation: Qt.Horizontal
                    width: wizard.steps.count * 80 - 80 + diameter
                    height: wizard.height
                }
                AnchorChanges {
                    target: stepsView;
                    anchors.right: parent.right;
                }
            },
            State {
                name: "vertical"
                PropertyChanges {
                    target: stepsView;
                    anchors.bottomMargin: 16
                    orientation: Qt.Vertical
                    width: wizard.width
                    height: wizard.steps.count * 80 - 80 + diameter
                }
                AnchorChanges {
                    target: stepsView;
                    anchors.bottom: parent.bottom;
                }
            }
        ]
    }

    function draw(type) {
        Mark.draw(type);
    }
}
