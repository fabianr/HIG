function draw(type) {
    // Add ruler
    var ruler = Qt.createComponent("../Ruler.qml");
    var brace = Qt.createComponent("../Brace.qml");
    var outline = Qt.createComponent("../Outline.qml");
    var messure = Qt.createComponent("../Messure.qml");

    switch(type) {
        case "overview":
            messure.createObject(root, {from: wizard, to: title });
            messure.createObject(root, {from: wizard, to: title, type: "top", rx: title.mapToItem(root, 0, 0).x + 16 });
            messure.createObject(root, {from: title, to: wizard, type: "bottom", rx: title.mapToItem(root, 0, 0).x + 16 });
            outline.createObject(root, {item: titleImg, label: false});

            outline.createObject(root, {item: stepsView});
            messure.createObject(root, {from: stepsView, to: wizard, type: "right"});
        break;
        case "step":
            outline.createObject(root, {item: stepsView.currentItem.bu, label: true});
            outline.createObject(root, {item: stepsView.currentItem.la});
            messure.createObject(root, {from: stepsView.currentItem, to: stepsView.currentItem.bu, ry: stepsView.currentItem.mapToItem(root, 0, 0).y + 8});
            messure.createObject(root, {from: wizard, to: stepsView.currentItem.bu, type: "top", rx: stepsView.currentItem.bu.mapToItem(root, 0, 0).x});
        break;
        case "label":
            ruler.createObject(root, {ry: wizard.mapToItem(root, 0, 0).y + wizard.height / 2});
            brace.createObject(root, {"from": titleImg, "to": titleLabel, "text": "16", "center": false});
            messure.createObject(root, {from: wizard, to: title });

        break;
        case "v-overview":
            ruler.createObject(root, {rx: titleImg.mapToItem(root, 0, 0).x, horizontal: false});
            messure.createObject(root, {from: wizard, to: title, type: "top", rx: title.mapToItem(root, 0, 0).x + 16 });
            messure.createObject(root, {from: wizard, to: titleImg});
            outline.createObject(root, {item: titleImg, label: false});
            ruler.createObject(root, {ry: titleImg.mapToItem(root, 0, 0).y + titleImg.height / 2});
            outline.createObject(root, {item: stepsView});
            messure.createObject(root, {from: stepsView, to: wizard, type: "bottom", rx: stepsView.mapToItem(root, 0, 0).x + stepsView.width * 2});
        break;
        case "v-step":
            ruler.createObject(root, {rx: titleImg.mapToItem(root, 0, 0).x, horizontal: false});
            outline.createObject(root, {item: stepsView.currentItem.bu, label: true});
            outline.createObject(root, {item: stepsView.currentItem.la});
            brace.createObject(root, {"from": stepsView.currentItem.bu, "to": stepsView.currentItem.la, "text": "16", "center": false});
            messure.createObject(root, {from: stepsView.currentItem, to: stepsView.currentItem.bu, type: "top", rx: stepsView.currentItem.mapToItem(root, 0, 0).x + 14});
            ruler.createObject(root, {ry: stepsView.currentItem.bu.mapToItem(root, 0, 0).y + stepsView.currentItem.bu.height / 2});
        break;
        case "v-label":
            messure.createObject(root, {from: wizard, to: title, type: "top", rx: title.mapToItem(root, 0, 0).x });
            messure.createObject(root, {from: wizard, to: titleImg, ry: titleImg.mapToItem(root, 0, 0).y + titleImg.height / 2});
            brace.createObject(root, {"from": titleImg, "to": titleLabel, "text": "16", "center": false});
            ruler.createObject(root, {ry: titleImg.mapToItem(root, 0, 0).y + titleImg.height / 2});
        break;

    }

    return;
}
