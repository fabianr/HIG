//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    id: root
    width: 300;
    height: 400;
    scale: 2

    Image {
        anchors.fill: parent
        id: bg
        source: "../img/breeze-wallpaper/1280x1024.png"
        fillMode: Image.PreserveAspectCrop
    }

    HIGWindow {
        x: 80
        y: -80
        //width: 400
        Wizard {
            id: wizard
            height: 400
            width: 200
            horizontal: false
            steps: ListModel {
                ListElement {
                   name: "Start"
                   src: ""
                }
                ListElement {
                   name: "Step 1 with a long title"
                   src: ""
                }
                ListElement {
                   name: "Step 2"
                   src: "../img/mail-message-new.svg"
                }
                ListElement {
                   name: "Step 3"
                   src: ""
                }
            }
            cur: 2
        }
        Rectangle {
            color: "#ccc"
            width: 500
            height: wizard.height
            anchors.left: wizard.right
        }
    }

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: 4
        color: "rgba(200, 200, 200, 0.2)"
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            wizard.draw("v-step");
            return;
        }
    }
}

