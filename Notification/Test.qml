import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

Rectangle {
    width: 400
    height: 400
    color: "white"
    property int ry : 5

    Rectangle {
        x: 20
        y: 20
        width: 40
        height: 40
        color: "yellow"
        id: canvas
        scale: 4
        transformOrigin: Item.TopLeft
        smooth: true

        /*Canvas {
            anchors.fill: parent;
            smooth: true
            onPaint: {
                // get scale because annotation should not be scaled
                var n = canvas.parent
                var ctx = getContext("2d");
                ctx.strokeStyle = "rgba(41,128,185, 1)"
                ctx.lineWidth = 1 / parent.scale
                ctx.beginPath();

                var xt = 0;
                while (xt < canvas.width) {
                    ctx.moveTo(xt, ry - ctx.lineWidth);
                    xt += units.smallSpacing;
                    ctx.lineTo(xt, ry - ctx.lineWidth);
                    xt += units.smallSpacing;
                }
                ctx.stroke();
             }
        }*/
        Row {
            id: hack
            anchors.fill: parent;
            spacing: units.smallSpacing / canvas.scale
            Repeater {
                   model: new Array(Math.floor(canvas.width / hack.spacing / 2))
                   Rectangle {
                       width: units.smallSpacing / canvas.scale
                       height: 1 / canvas.scale
                       color: "#2980b9"
                   }
            }
        }
    }
}
