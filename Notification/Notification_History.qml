//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles.Plasma 2.0 as PlasmaStyles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

//window containing the application
Rectangle {
    width: 800
    height: 500
    id: root
    property var model : Data{}

    Image {
        anchors.fill: parent
        id: bg
        source: "../img/breeze-wallpaper/1280x1024.png"
        fillMode: Image.PreserveAspectCrop
    }

    Rectangle {
        color: "#90bdc3c7"
        x: 0
        y: 0
        width: 280
        height: parent.height

        ScrollView {
            width: parent.width - 4
            height: 400
            y: 30
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            NotificationStack {
                width: parent.parent.width - 4;
                model: root.model
            }
            style: PlasmaStyles.ScrollViewStyle{}
        }
    }


    Rectangle {
        color: "#eebdc3c7"
        id: plasmoid
        x: 400
        y: 100
        height: childrenRect.height + 8;
        width: 350
        radius: 3;
        ScrollView {
            width: parent.width - 4
            height: 300
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

            NotificationStack {
                x: 4
                y: 4
                width: parent.parent.width - 8
                model: root.model
            }
            style: PlasmaStyles.ScrollViewStyle{}
        }
    }
    DropShadow {

       anchors.fill: plasmoid
       horizontalOffset: 5
       verticalOffset: 5
       radius: 8.0
       samples: 17
       color: "#cc666666"
       source: plasmoid
   }

    Label {
        x: 400
        y: 20;
        text: "notification history in sidebar and plasmoid"
        color: "white"
    }
}
