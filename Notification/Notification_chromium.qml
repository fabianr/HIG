//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore

//window containing the application
Rectangle {
    width: 520
    height: 360
    id: root


    property int base: 4
    property real goldenRatioA: 0.618
    property real goldenRatioB: 0.382

    Image {
        anchors.fill: parent
        id: bg
        source: "../img/breeze-wallpaper/1280x1024.png"
        fillMode: Image.PreserveAspectCrop
    }

    Notification {
        x: 20
        y: 20
        appTitle: "Chomium"
        appIcon: "../img/chromium.png"
        age: "1 hour ago"
        Rectangle {
            width: 480
            height: 120;
            Image {
                anchors.right: parent.right;
                anchors.bottom: parent.bottom;
                anchors.rightMargin: 32;
                anchors.bottomMargin: 8
                height: 64;
                width: 64;
                source: "../img/so-icon.svg"
                smooth: true
            }

			ColumnLayout {
                x: 20;
                Row {

                    Label {
                        text: "Notification title"
                        font.pixelSize: 22
                    }
                }
				Label {
                    text: "<a href='http://null.jsbin.com'>null.jsbin.com</a>"
                    color: "#2980b9"
				}
				Label {
                    text: "Hey there!"
				}
                Label {
                    text: "You have been notified!"
                }
			}
        }

    }

    Notification2 {
        x: 20
        y: 180
        appTitle: "Chomium"
        appIcon: "../img/chromium.png"
        age: "1 hour ago"
        Rectangle {
            width: 480
            height: 120;

            ColumnLayout {
                x: 20;
                Row {
                    Image {
                        height: 32;
                        width: 32;
                        source: "../img/so-icon.svg"
                        smooth: true
                    }

                    Label {
                        text: "Notification title"
                        font.pixelSize: 22
                    }
                }
                Label {
                    text: "<a href='http://null.jsbin.com'>null.jsbin.com</a>"
                    color: "#2980b9"
                }
                Label {
                    text: "Hey there!"
                }
                Label {
                    text: "You have been notified!"
                }
            }
        }

    }

}
