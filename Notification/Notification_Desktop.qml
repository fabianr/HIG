//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as PlasmaStyles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

import "notification.js" as N

//window containing the application
Rectangle {
    width: 1024
    height: 768
    id: root
    property var history : Data{}
    property int n: 0;
    property var popup : Data{}

    Image {
        anchors.fill: parent
        id: bg
        source: "../img/breeze-wallpaper/1280x1024.png"
        fillMode: Image.PreserveAspectCrop
    }

    Rectangle {
        color: "#90bdc3c7"
        x: 0
        y: 0
        width: 280
        height: parent.height
        Label {
            x: 4
            y: 4
            text: "History in a sidebar"
            color: "white"
        }

        ScrollView {
            width: parent.width - 4
            height: 400
            y: 30
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            NotificationStack {
                width: parent.parent.width - 4;
                model: root.history
                onDeleteNotification: root.history.deleteNotification(idxnot, idxgrp)
            }
            style: PlasmaStyles.ScrollViewStyle{}
        }
    }


    Label {
        x: 400
        y: 76
        text: "History in a plasmoid"
        color: "white"
    }
    Rectangle {
        color: "#eebdc3c7"
        x: 400
        y: 100
        height: childrenRect.height + 8;
        width: 350

        ScrollView {
            width: parent.width - 4
            height: 300
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff

            NotificationStack {
                x: 4
                y: 4
                width: parent.parent.width - 8
                model: root.history
                onDeleteNotification: root.history.deleteNotification(idxnot, idxgrp)
            }
            style: PlasmaStyles.ScrollViewStyle{}
        }
    }

    // Bottom Panel
    Rectangle {
        id: panel
        color: "#90bdc3c7"
        width: parent.width
        height: 32
        anchors.left: parent.left
        anchors.bottom: parent.bottom

        Image {
            id: panelIcon
            anchors.right: parent.right;
            anchors.bottom: parent.bottom;
            anchors.rightMargin: 5;
            anchors.bottomMargin: 5;
            height: 22;
            width: 22;
            source: "../img/help-about.svg"
            smooth: true
            z: 4
        }
    }
    // Popup
    Item {
        //color: "#eebdc3c7"
        id: popupgroup
        anchors.right: parent.right
        anchors.bottom: panel.top
        anchors.margins: 8
        visible: root.popup.count > 0

        height: childrenRect.height + 16;
        width: 300

        /*ScrollView {
            width: parent.width - 4
            height: 400;
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            anchors.bottom: parent.bottom*/
            NotificationStack {

                x: 4
                y: 4
                width: popupgroup.width - 8
                model: root.popup
                popup: true
                onDeleteNotification: root.popup.deleteNotification(idxnot, idxgrp)
                anchors.bottom: parent.bottom
            }
            //style: PlasmaStyles.ScrollViewStyle{}
        //}
    }


    Label {
        x: 400
        y: 76
        text: "Next"
        color: "white"
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 16
        MouseArea {
            anchors.fill: parent
            onClicked: {
                var group = N.moreNotifications[root.n]
                // Popup
                root.popup.addNotification(group)
                // History
                if (!("urgency" in group["notifications"][0] && roup["notifications"][0].urgency === 0)) {
                    root.history.addNotification(group)
                }
                root.n++
            }
        }
    }

    Component.onCompleted: function() {
       //root.history.clear();
       root.popup.clear();

       /*for (var i = 0; i < N.startNotifications.length; i++) {
            root.history.appendGroup(N.startNotifications[i]);
       }
       console.log(root.history.get(0).notifications.count);*/
    }

}
