//import related modules
import QtQuick 2.7
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0

//window containing the application
Rectangle {
    id: root
    default property alias data : windowContent.data
	property string appTitle;
	property string appIcon;
    property ListModel actions
    height: root.actions && root.actions.count > 0 ? childrenRect.height + 48 : childrenRect.height;
    width: childrenRect.width;
    property string age;

    DropShadow {
       anchors.fill: root
       horizontalOffset: 5
       verticalOffset: 5
       radius: 8.0
       samples: 17
       color: "#25666666"
       source: root
   }

    Rectangle {
        id: header
        height: 32
        width: root.width

        Row {
            anchors.left: header.left
            anchors.leftMargin: 20
            height: parent.height - 4
            y: 8
            spacing: 8
            Image {
                height: parent.height - 8;
                width: parent.height - 8;
                source: root.appIcon
                smooth: true
            }
            Label {
				color: "#7f8c8d"
                id: appTitle
				text: root.appTitle;
			}
            Item {
                width: 20
            }

            Label {
                color: "#7f8c8d"
                font.pixelSize: 12
                text: root.age
                y: 2
            }
        }

        Row {
            anchors.right: header.right
            anchors.rightMargin: 8
            height: parent.height - 4
            y: 8
            spacing: 8
            /*Image {
                height: parent.height - 8;
                width: parent.height - 8;
                source: root.appIcon
                smooth: true
            }
            Label {
                color: "#7f8c8d"
                text: root.appTitle;
            }*/
            Image {
                height: parent.height - 8;
                width: parent.height - 8;
                source: "../img/configure-shortcuts.svg"
                smooth: true
            }
            Image {
                height: parent.height - 8;
                width: parent.height - 8;
                source: "../img/window-close.svg"
                smooth: true
            }
        }
    }
    Item {
        id: windowContent
        anchors.top: header.bottom
        anchors.left: root.left
        height: childrenRect.height;
        width: childrenRect.width;
    }

    Rectangle {
        height: root.actions && root.actions.count > 0 ? 48 : 0;
        width: parent.width;
        anchors.bottom: parent.bottom;
        id: cmds
        visible: root.actions && root.actions.count > 0
        color: "#eff0f1"

        Row {
            y: 12
            anchors.verticalCenter: parent.Center;
            x: 20;
            Repeater {
                model: root.actions
                Rectangle {
                    height: 24
                    width: childrenRect.width + 32
                    color: "#eff0f1"
                    Row {
                        spacing: 8
                        Image {
                            source: model.source
                            height: 24
                            width: 24
                            smooth: true
                        }
                        Label {
                            text: model.text
                        }
                    }
                }
            }
        }
        Canvas {
            anchors.fill: cmds;
            id: canvas
            onPaint: {
                return;
                var ctx = getContext("2d");
                ctx.strokeStyle = canvas.color;
                ctx.beginPath();
                ctx.moveTo(0, 0);
                ctx.lineTo(canvas.width, 0);
                ctx.stroke();
            }
        }
    }

}
