//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

//window containing the application
Rectangle {
    width: 1040
    height: 400
    id: root

    Image {
        anchors.fill: parent
        id: bg
        source: "../img/breeze-wallpaper/1280x1024.png"
        fillMode: Image.PreserveAspectCrop
    }

    NotificationGroup {
        x: 20
        y: 20
        appTitle: "E-Mail notifications"
		appIcon: "../img/mail-client.svg"
        age: "5 min ago"
        /*actions: ListModel {
            ListElement {source: "../img/mail-reply-sender.svg"; text: "Replay"}
            ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"}
        }*/

        Column {
            id: msgs
            width: 480
            Rectangle {
                width: 480
                height: childrenRect.height + 32;
                border.width: 1
                border.color: "#bdc3c7"
                ColumnLayout {
                    x: 20;
                    y: 16
                    Label {
                        text: "From: joe@doe.com"
                    }
                    Label {
                        text: "Subject: some new mail"
                        font.pixelSize: 16
                    }
                }
                Image {
                    height: 16;
                    width: 16;
                    source: "../img/go-down.svg"
                    smooth: true
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.margins: 8
                }

            }
            Rectangle {
                width: 480
                height: childrenRect.height + 32;
                border.width: 1
                border.color: "#bdc3c7"
                ColumnLayout {
                    x: 20;
                    y: 16
                    Label {
                        text: "From: jane@doe.com"
                    }
                    Label {
                        text: "Subject: some more mail"
                        font.pixelSize: 16
                    }
                }
                Image {
                    height: 16;
                    width: 16;
                    source: "../img/go-down.svg"
                    smooth: true
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.margins: 8
                }
            }
            Rectangle {
                width: 480
                height: childrenRect.height + 32;
                border.width: 1
                border.color: "#bdc3c7"
                ColumnLayout {
                    x: 20;
                    y: 16
                    Label {
                        text: "From: somebody@doe.com"
                    }
                    Label {
                        text: "Subject: even more"
                        font.pixelSize: 16
                    }
                }
                Image {
                    height: 16;
                    width: 16;
                    source: "../img/go-down.svg"
                    smooth: true
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.margins: 8
                }
            }

        }
    }

    NotificationGroup {
        x: 520
        y: 20
        appTitle: "E-Mail notifications"
        appIcon: "../img/mail-client.svg"
        age: "5 min ago"
        /*actions: ListModel {
            ListElement {source: "../img/mail-reply-sender.svg"; text: "Replay"}
            ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"}
        }*/

        Column {
            width: 480
            Rectangle {
                width: 480
                height: childrenRect.height + 32;
                border.width: 1
                border.color: "#bdc3c7"
                ColumnLayout {
                    x: 20;
                    y: 16
                    Label {
                        text: "From: joe@doe.com"
                    }
                    Label {
                        text: "Subject: some new mail"
                        font.pixelSize: 16
                    }
                }

                Image {
                    height: 16;
                    width: 16;
                    source: "../img/go-down.svg"
                    smooth: true
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.margins: 8
                }
            }
            Rectangle {
                width: 480
                height: childrenRect.height + 48;
                border.width: 1
                border.color: "#bdc3c7"
                ColumnLayout {
                    x: 20;
                    y: 16
                    Label {
                        text: "From: jane@doe.com"
                    }
                    Label {
                        text: "Subject: some more mail"
                        font.pixelSize: 16
                    }
                    Label {
                        text: "In: inbox"
                    }
                }
                Image {
                    height: 16;
                    width: 16;
                    source: "../img/go-up.svg"
                    smooth: true
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.rightMargin: 8;
                    anchors.bottomMargin: 56
                }
                Rectangle {
                    height: 48;
                    width: parent.width;
                    anchors.bottom: parent.bottom;
                    id: cmds
                    color: "#eff0f1"

                    Row {
                        y: 12
                        anchors.verticalCenter: parent.Center;
                        x: 20;
                        Repeater {
                            model: ListModel {
                                ListElement {source: "../img/mail-reply-sender.svg"; text: "Replay"}
                                ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"}
                            }
                            Rectangle {
                                height: 24
                                width: childrenRect.width + 32
                                color: "#eff0f1"
                                Row {
                                    spacing: 8
                                    Image {
                                        source: model.source
                                        height: 24
                                        width: 24
                                        smooth: true
                                    }
                                    Label {
                                        text: model.text
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Rectangle {
                width: 480
                height: childrenRect.height + 32;
                border.width: 1
                border.color: "#bdc3c7"
                ColumnLayout {
                    x: 20;
                    y: 16
                    Label {
                        text: "From: somebody@doe.com"
                    }
                    Label {
                        text: "Subject: even more"
                        font.pixelSize: 16
                    }
                }
                Image {
                    height: 16;
                    width: 16;
                    source: "../img/go-down.svg"
                    smooth: true
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.margins: 8
                }
            }

        }
    }

}
