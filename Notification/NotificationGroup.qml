//import related modules
import QtQuick 2.7
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0

import "HIG.js" as H

//window containing the application
Rectangle {
    visible: root.notifications.count > 0

    id: root
	property string appTitle;
	property string appIcon;
    property ListModel notifications
    property string age;
    property bool popup : false;
    property bool expanded : false;
    property ListModel groups
    signal deleteNotification(int idxnot, int idxgrp)
    border.color: "#bdc3c7"
    border.width: 1
    color: "#eff0f1"

    radius: 3;
    height: childrenRect.height;
    width: 300

    Item {
        id: header
        height: units.smallSpacing * 7
        width: root.width


        Row {
            anchors.left: header.left
            anchors.leftMargin: units.smallSpacing * 2
            anchors.verticalCenter: header.verticalCenter
            height: units.iconSizes.smallMedium
            spacing: units.smallSpacing * 2

            Image {
                id: appIcon
                height: units.iconSizes.smallMedium
                width: units.iconSizes.smallMedium;
                source: root.appIcon
                smooth: true
            }
            /*Image {
                height: parent.height - 8;
                width: parent.height - 8;
                source: "../img/configure-shortcuts.svg"
                smooth: true
            }*/
            Label {
				color: "#7f8c8d"
                id: appTitle
				text: root.appTitle;
                font.pixelSize: 12
                anchors.verticalCenter: parent.verticalCenter
                renderType: Text.QtRendering
			}
            /*Item {
                width: 8
                height: 16
            }
            Label {
                visible: root.notifications.count > 2
                color: "#da4453"
                text: "+" + (root.notifications.count - 2)
            }
            Image {
                visible: root.notifications.count > 2
                height: 16;
                width: 16;
                source: "../img/go-down.svg"
                smooth: true
                y: 4
            }*/
        }



        Row {
            anchors.right: header.right
            anchors.rightMargin: units.smallSpacing * 2
            anchors.verticalCenter: header.verticalCenter
            height: units.iconSizes.smallMedium
            spacing: units.smallSpacing * 2
            /*Image {
                height: parent.height - 8;
                width: parent.height - 8;
                source: root.appIcon
                smooth: true
            }
            Label {
                color: "#7f8c8d"
                text: root.appTitle;
            }*/
            Label {
                id: lblAge
                visible: root.notifications.count <= 1
                color: "#7f8c8d"
                font.pixelSize: 12
                text: root.age
                anchors.verticalCenter: parent.verticalCenter
                renderType: Text.QtRendering
            }
            Label {
                id: more
                visible: !root.expanded && root.notifications.count > 2
                color: "#da4453"
                text: "+" + (root.notifications.count - 2)
                renderType: Text.QtRendering
            }
            Label {
                id: cnt
                visible: root.expanded && root.notifications.count > 2
                color: "#7f8c8d"
                text: root.notifications.count
                renderType: Text.QtRendering
            }
            Image {
                id: expandIcon
                visible: !root.expanded && root.notifications.count > 2
                height: units.iconSizes.small
                width: units.iconSizes.small
                source: "../img/go-down.svg"
                smooth: true
                anchors.verticalCenter: parent.verticalCenter
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                       root.expanded = true
                    }
                }
            }
            Image {
                visible: root.expanded && root.notifications.count > 2
                height: units.iconSizes.small
                width: units.iconSizes.small
                source: "../img/go-up.svg"
                smooth: true
                anchors.verticalCenter: parent.verticalCenter
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                       root.expanded = false
                    }
                }
            }


            Image {
                id: imgClose
                height: units.iconSizes.smallMedium
                width: units.iconSizes.smallMedium
                source: "../img/window-close.svg"
                smooth: true
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                       root.groups.remove(root.index)
                    }
                }
            }
        }
    }


    Column {
        y: header.height
        x: 0;
        id: content
        width: root.width
        Repeater {
            model: root.notifications
            id: body
            NotificationBody {
                nData: model
                visible: root.expanded || index < 2
                grouped: root.notifications.count > 1
                popup: root.popup
                notifications: root.notifications
                onDeleteNotification: deleteNotGrp(idxnot)
            }
        }
    }

    function deleteNotGrp(idxnot) {
        //console.log(idxnot + " " + index)
        root.deleteNotification(idxnot, index)
    }

    /*Rectangle {
        id: footer
        visible: root.notifications.count > 2
        anchors.top: content.bottom;
        width: parent.width
        height: root.notifications.count > 2? 32 : 0
        Label {
            x: 20
            y: 4
            text: "<font color='#da4453'> +" + (root.notifications.count - 2) + "</font> more new messages"
        }

        Canvas {
            anchors.fill: footer;
            id: canvas
            onPaint: {
                var ctx = getContext("2d");
                ctx.strokeStyle = "rgba(127,140,141,1)";
                ctx.beginPath();
                ctx.moveTo(0, 0);
                ctx.lineTo(canvas.width, 0);
                ctx.stroke();
            }
        }
    }*/

    function draw(type) {
        H.draw(type);
    }
    function drawBody(type) {
        body.itemAt(0).draw(type)
    }
}
