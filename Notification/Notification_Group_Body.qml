//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtGraphicalEffects 1.0

import "HIG.js" as H
import "notification.js" as N

//window containing the application
Item {
    width: 660
    height: 280

    Notification_Group {
        x: 0
        y: -100
        width: 660
        height: 280
        typesGrp: ["gstructur-group"]
        typesBdy: ["gbody-align", "gbody-text"]
        scl: 2
        grid: false
        content: N.groupNoty3
    }

    BaselineGrid {
        z: 1
        base: units.smallSpacing * 2
        color: "rgba(0, 0, 0, 0.2)"
        visible: root.grid
    }
}


