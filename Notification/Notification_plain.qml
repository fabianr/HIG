//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore

//window containing the application
Rectangle {
    width: 520
    height: 180
    id: root


    property int base: 4
    property real goldenRatioA: 0.618
    property real goldenRatioB: 0.382

    Image {
        anchors.fill: parent
        id: bg
        source: "../img/breeze-wallpaper/1280x1024.png"
        fillMode: Image.PreserveAspectCrop
    }

    Notification {
        x: 20
        y: 20
        appTitle: "E-Mail notification"
		appIcon: "../img/mail-client.svg"
        Rectangle {
            width: 480
            height: 120;
			ColumnLayout {
				x: 40;
				Label {
					text: "From: joe@doe.com"
				}
				Label {
					text: "Subject: some new mail"
				}
				Label {
					text: "In: inbox"
				}
			}
            Rectangle {
                height: 48;
                width: parent.width;
                anchors.bottom: parent.bottom;

                Row {
                    spacing: 8
                    anchors.verticalCenter: parent.Center;
                    x: 40;
                    ToolButton {
                        text: "Reply"
                    }
                    ToolButton {
                        text: "Delete"
                    }
                }
            }
        }

    }
}
