//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents


Column {
    property var model
    property bool popup: false
    id: root
    width: 300
    bottomPadding: 16
    signal deleteNotification(int idxnot, int idxgrp)

    Repeater {
        model: root.model
        id: repeat
        Item {
            height: childrenRect.height + 16;
            width: root.width

            NotificationGroup {
                appTitle: model.appTitle
                appIcon: model.appIcon
                age: model.age
                width: parent.width
                notifications: model.notifications
                popup: root.popup
                groups: root.model
                expanded: model.expanded
                onDeleteNotification: deleteNotStack(idxnot, idxgrp)
            }
        }
    }

    function deleteNotStack(idxnot, idxgrp) {
        root.deleteNotification(idxnot, idxgrp)
    }

    function draw(type) {
        repeat.itemAt(0).children[0].draw(type);
    }
    function drawBody(type) {
        repeat.itemAt(0).children[0].drawBody(type)
    }
}
