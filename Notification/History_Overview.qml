//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtGraphicalEffects 1.0

import "HIG.js" as H
import "notification.js" as N

//window containing the application
Column {
    Row {
        height: 160
        spacing: 0
        History_Group {
            typesGrp: []
            content: N.singleNoty6
        }

        History_Group {
            typesGrp: []
            content: N.groupNoty5
        }
    }
    Row {
        height: 240
        spacing: 0
        History_Group {
            typesGrp: []
            content: N.singleNoty5
        }

        History_Group {
            typesGrp: []
            content: N.groupNoty1
        }

    }
}
