//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtGraphicalEffects 1.0

import "HIG.js" as H
import "notification.js" as N

//window containing the application
Item {
    width: 360
    height: 160

    Notification_Single {
        width: 300
        typesGrp: ["body-align"]
        typesBdy: ["body-text"]
        actions: [];
        scl: 1
        grid: false
        content: N.singleNoty4
        x: -10
        y: -10
    }

    // Draw helpers and anotation
    /*BaselineGrid {
        z: 1
        //color: "rgba(0, 0, 0, 0.1)"
        visible: true
    }*/
}
