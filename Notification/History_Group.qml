//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtGraphicalEffects 1.0

import "HIG.js" as H
import "notification.js" as N

//window containing the application
Rectangle {
    width: 360
    height: 220
    id: root
    property int scl: 1
    property var typesGrp : []
    property var typesBdy : []
    property bool grid: false
    property bool actions : true
    property var noties : Data{}
    property var content : N.groupNoty1

    Item {
        id: container
        width:  noty.width * noty.scale
        height: noty.height * noty.scale
        //visible: false
        x: 30
        y: 30

        NotificationStack {
            scale: root.scl
            transformOrigin: Item.TopLeft
            smooth: true
            popup: false
            id: noty
            x: 0
            y: 0
            model: root.noties
            onDeleteNotification: root.history.deleteNotification(idxnot, idxgrp)
        }

        /*NotificationGroup {
            scale: root.scl
            transformOrigin: Item.TopLeft
            smooth: true
            id: noty
            x: rectShadow.radius
            y: rectShadow.radius

            appTitle: "Chomium"
            appIcon: "../img/chromium.png"
            age: "5 min ago"
            popup: true
            notifications: ListModel {
                ListElement {
                    summary: "Notification title"
                    body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!"
                    icon: "../img/so-icon.svg"
                    age: "5 min ago"
                    expandable: true
                    expanded: true
                    actions: [
                        ListElement {source: "../img/bookmark-new.svg"; text: "Bookmark"},
                        ListElement {source: "../img/delete-comment.svg"; text: "Unsubscribe"}
                    ]
                }
            }
        }*/
    }



    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: units.smallSpacing * 2
        color: "rgba(0, 0, 0, 0.2)"
        visible: root.grid
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            for (var i = 0; i < root.typesGrp.length; i++) {
                noty.draw(root.typesGrp[i]);
            }
            for (i = 0; i < root.typesBdy.length; i++) {
                noty.drawBody(root.typesBdy[i]);
            }
            return;
        }
    }

    Component.onCompleted: function() {
       root.noties.clear();
       root.noties.addNotification(root.content)
       /*for (var size in units.iconSizes) {
            console.log(size + " " + units.iconSizes[size])
           //console.log(size)
       }*/
    }
}
