//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtGraphicalEffects 1.0

import "HIG.js" as H
import "notification.js" as N

//window containing the application
Item {
    width: 640
    height: 220

    Notification_Single {
        width: 640
        height: 220
        typesGrp: ["body-align"]
        typesBdy: ["action"]
        scl: 2
        grid: false
        y: -180
        content: N.singleNoty3
    }

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: 8
        //color: "rgba(0, 0, 0, 0.1)"
        visible: true
    }
}
