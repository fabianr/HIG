//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

//window containing the application
Rectangle {
    width: 1000
    height: 350
    id: root

    Image {
        anchors.fill: parent
        id: bg
        source: "../img/breeze-wallpaper/1280x1024.png"
        fillMode: Image.PreserveAspectCrop
    }

    Row {
        x: 20
        y: 20
        spacing: 20

        Column {
            spacing: 20


            NotificationGroup {
                appTitle: "Chomium"
                appIcon: "../img/chromium.png"
                age: "5 min ago"
                popup: true
                notifications: ListModel {
                    ListElement {
                        summary: "Notification title"
                        body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!"
                        icon: "../img/so-icon.svg";
                        age: "5 min ago"
                        expandable: true
                        expanded: true
                    }
                }
            }

            NotificationGroup {
                appTitle: "E-Mail"
                appIcon: "../img/mail-client.svg"
                age: "10 min ago"
                popup: true

                notifications: ListModel {
                    ListElement {
                        summary: "jane@doe.com"
                        body: "some new mail<br/>In: Inbox"
                        age: "20 min ago"
                        expandable: true
                        expanded: true
                        actions: [
                            ListElement {source: "../img/mail-reply-sender.svg"; text: "Reply"},
                            ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"}
                        ]
                    }
                }
            }

        }

        NotificationGroup {
            appTitle: "Chomium"
            appIcon: "../img/chromium.png"
            age: "5 min ago"
            popup: true
            notifications: ListModel {
                /*ListElement {
                    summary: "Notification title"
                    body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!"
                    icon: "../img/so-icon.svg";
                    age: "5 min ago"
                    expandable: true
                    expanded: true
                }*/
                ListElement {
                    summary: "Notification title"
                    body: "Some more text with icon"
                    icon: "../img/so-icon.svg";
                    age: "1 hour ago"
                    expandable: true
                    expanded: true
                }
                ListElement {
                    summary: "Notification title"
                    body: "Some more text without icon"
                    age: "2 hour ago"
                    expandable: true
                    expanded: true
                }

            }
        }

        NotificationGroup {
            appTitle: "E-Mail"
            appIcon: "../img/mail-client.svg"
            age: "10 min ago"
            popup: true
            notifications: ListModel {
                ListElement {
                    summary: "jane@doe.com"
                    body: "some new mail<br/>In: Inbox"
                    age: "10 min ago"
                    expandable: true
                    expanded: true
                    icon: "../img/woman_headphone.jpg"
                    actions: [
                        ListElement {source: "../img/mail-reply-sender.svg"; text: "Reply"},
                        ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"}
                    ]
                }
                ListElement {
                    summary: "joe@doe.com"
                    body: "some new mail<br/>In: Inbox"
                    age: "20 min ago"
                    expandable: true
                    expanded: true
                    actions: [
                        ListElement {source: "../img/mail-reply-sender.svg"; text: "Reply"},
                        ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"}
                    ]
                }
                ListElement {
                    summary: "jane@doe.com"
                    body: "some new mail<br/>In: Inbox"
                    age: "30 min ago"
                    expandable: true
                    expanded: true
                }


            }
        }
    }
}
