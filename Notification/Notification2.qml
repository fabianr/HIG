//import related modules
import QtQuick 2.7
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore

//window containing the application
Rectangle {
    id: root
    default property alias data : windowContent.data
    property ListModel actions
	property string appTitle;
	property string appIcon;
    property string age;

    height: root.actions && root.actions.count > 0 ? childrenRect.height + 32 : childrenRect.height;
    width: childrenRect.width;

    DropShadow {
       anchors.fill: parent
       horizontalOffset: 5
       verticalOffset: 5
       radius: 16.0
       samples: 17
       color: "#25666699"
       source: root
   }

    Rectangle {
        id: header
        height: 32
        z: 3
        anchors.top: parent.top;
        anchors.right: parent.right

        Row {
            anchors.right: header.right
            anchors.rightMargin: 8
            height: parent.height - 4
            y: 8
            spacing: 8
            /*Image {
                height: parent.height - 8;
                width: parent.height - 8;
                source: root.appIcon
                smooth: true
            }
            Label {
                color: "#7f8c8d"
                text: root.appTitle;
            }*/
            Label {
                font.pixelSize: 12
                text: root.age
                y: 2
                color: "#7f8c8d"
            }
            Item {
                width: 20
            }

            Image {
                height: parent.height - 8;
                width: parent.height - 8;
                source: "../img/configure-shortcuts.svg"
                smooth: true
            }
            Image {
                height: parent.height - 8;
                width: parent.height - 8;
                source: "../img/window-close.svg"
                smooth: true
            }
        }
    }
    Item {
        id: windowContent
        anchors.top: header.top
        anchors.topMargin: 8
        anchors.left: root.left
        height: childrenRect.height;
        width: childrenRect.width;
    }

    Row {
        anchors.right: windowContent.right
        anchors.bottom: windowContent.bottom
        anchors.rightMargin: 8
        height: 32
        spacing: 8
        Image {
            height: parent.height - 8;
            width: parent.height - 8;
            source: root.appIcon
            smooth: true
        }
        Label {
            color: "#7f8c8d"
            text: root.appTitle;
        }
    }
    Rectangle {
        height: root.actions && root.actions.count > 0 ? 32 : 0;
        width: parent.width;
        anchors.bottom: parent.bottom;
        id: cmds
        visible: root.actions && root.actions.count > 0

        Row {
            y: 4
            anchors.verticalCenter: parent.Center;
            x: 20;
            Repeater {
                model: root.actions
                Rectangle {
                    height: 24
                    width: childrenRect.width + 20
                    Row {
                        spacing: 8
                        Image {
                            source: model.source
                            height: 24
                            width: 24
                            smooth: true
                        }
                        Label {
                            text: model.text
                        }
                    }
                }
            }
        }
        Canvas {
            anchors.fill: cmds;
            id: canvas
            onPaint: {
                var ctx = getContext("2d");
                ctx.strokeStyle = canvas.color;
                ctx.beginPath();
                ctx.moveTo(0, 0);
                ctx.lineTo(canvas.width, 0);
                ctx.stroke();
            }
        }
    }

}
