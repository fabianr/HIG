//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

//window containing the application
Rectangle {
    width: 520
    height: 400
    id: root

    property int base: 4
    property real goldenRatioA: 0.618
    property real goldenRatioB: 0.382

    Image {
        anchors.fill: parent
        id: bg
        source: "../img/breeze-wallpaper/1280x1024.png"
        fillMode: Image.PreserveAspectCrop
    }

    Notification {
        x: 20
        y: 20
        appTitle: "E-Mail notification"
		appIcon: "../img/mail-client.svg"
        age: "5 min ago"
        actions: ListModel {
            ListElement {source: "../img/mail-reply-sender.svg"; text: "Replay"}
            ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"}
        }

        Rectangle {
            width: 480
            height: childrenRect.height + 8;
			ColumnLayout {
                x: 20;
				Label {
					text: "From: joe@doe.com"
				}
				Label {
					text: "Subject: some new mail"
                    font.pixelSize: 16
				}
				Label {
					text: "In: inbox"
				}
			}
        }
    }

    Notification2 {
        x: 20
        y: 200
        appTitle: "E-Mail notification"
        appIcon: "../img/mail-client.svg"
        age: "5 min ago"
        actions: ListModel {
            ListElement {source: "../img/mail-reply-sender.svg"; text: "Replay"}
            ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"}
        }
        Rectangle {
            width: 480
            height: childrenRect.height + 8;

            ColumnLayout {
                x: 20;
                Label {
                    text: "From: joe@doe.com"
                }
                Label {
                    text: "Subject: some new mail"
                    font.pixelSize: 16
                }
                Label {
                    text: "In: inbox"
                }
            }

        }
    }

}
