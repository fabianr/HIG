//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtGraphicalEffects 1.0

import "HIG.js" as H
import "notification.js" as N

//window containing the application
Notification_Group {
    width: 640
    height: 180
    typesGrp: ["ggroup-header-1"]
    typesBdy: []
    scl: 2
    grid: true
    content: N.groupNoty3
}
