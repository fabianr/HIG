function nHeight() {
    var data = root.nData;
    var min;
    if (data.expanded || root.popup) {
        if ("actions" in data && data.actions !== undefined && data.actions.count > 0) {
            //console.log("expanded with actions")
            if ("icon" in data && data.icon !== undefined) {
                if (root.grouped || !root.popup) {
                    min = units.smallSpacing * 2 + closeIcon.height + units.smallSpacing * 2 + notificationIcon.height + units.smallSpacing * 2 + cmds.height
                    //console.log("expanded, grouped with actions and icon "  + min);
                }
                else {
                    min = textCont.y + notificationIcon.height + units.smallSpacing * 2 + cmds.height
                    //console.log("expanded, ungrouped with actions and icon "  + min);
                }
                return Math.max(textCont.height + cmds.height + units.smallSpacing * 2, min); // TODO calcularte 120
            }
            return textCont.height + cmds.height + textCont.y + units.smallSpacing * 2
        }
        if ("icon" in data && data.icon !== undefined) {
            if (parent.grouped || !root.popup) {
                min = textCont.y + closeIcon.height + units.smallSpacing * 2 + notificationIcon.height + units.smallSpacing * 2;
            }
            else {
                min = textCont.y + notificationIcon.height + units.smallSpacing * 2;
            }
            //console.log("expanded with icon " + min)
            return Math.max(textCont.height + textCont.y + units.smallSpacing * 2, min); // TODO calcularte 80
        }
        //console.log("expanded without icon")
        return textCont.height + textCont.y + units.smallSpacing * 2
    }
    else {
        //console.log("unexpanded")
        return abstr.height + textCont.y + units.smallSpacing * 2
    }

    //Math.max((root.nData.icon !== undefined ? ("expanded" in root.nData && root.nData.expanded ? 120 : 60)  : 60), "expanded" in root.nData && root.nData.expanded ? )
}

var singleNoty = [
        {
            appTitle: "E-Mails",
            appIcon: "../img/mail-client.svg",
            age: "10 min ago",
            notifications: [
                {
                    summary: "jane@doe.com",
                    body: "some new mail<br/>In: Inbox",
                    age: "10 min ago",
                    expanded: true,
                    icon: "../img/woman_headphone.jpg",
                    actions: [
                        {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                        {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                    ],
                    timeout: 0
                }
        ]
    }
]

var singleNoty2 = [
        {
            appTitle: "Chomium",
            appIcon: "../img/chromium.png",
            age: "5 min ago",
            notifications: [
                {
                    summary: "Notification title",
                    body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!",
                    age: "10 min ago",
                    expanded: true,
                    icon: "../img/so-icon.svg",
                    actions: [
                        {source: "../img/bookmark-new.svg", text: "Bookmark"},
                    ],
                    timeout: 0
                }
        ]
    }
]

var singleNoty3 = [
        {
            appTitle: "Chomium",
            appIcon: "../img/chromium.png",
            age: "5 min ago",
            notifications: [
                {
                    summary: "Notification title",
                    body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!",
                    age: "10 min ago",
                    expanded: true,
                    icon: "../img/so-icon.svg",
                    actions: [
                        {source: "../img/bookmark-new.svg", text: "Bookmark"},
                        {source: "../img/delete-comment.svg", text: "Unsubscribe"},
                    ],
                    timeout: 0
                }
        ]
    }
]

var singleNoty4 = [
        {
            appTitle: "Chomium",
            appIcon: "../img/chromium.png",
            age: "5 min ago",
            notifications: [
                {
                    summary: "Notification title",
                    body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!",
                    age: "10 min ago",
                    expanded: true,
                    icon: "../img/so-icon.svg",
                    timeout: 0
                }
        ]
    }
]

var singleNoty5 = [
        {
            appTitle: "Chomium",
            appIcon: "../img/chromium.png",
            age: "5 min ago",
            notifications: [
                {
                    summary: "Notification title",
                    body: "<a href='http://null.jsbin.com'>null.jsbin.com</a>",
                    age: "10 min ago",
                    expanded: true,
                    timeout: 0
                }
        ]
    }
]

var singleNoty6 = JSON.parse(JSON.stringify(singleNoty5));
singleNoty6[0].notifications[0].expanded = false

var groupNoty1 = [
        {
            appTitle: "Chomium",
            appIcon: "../img/chromium.png",
            age: "5 min ago",
            notifications: [
                {
                    summary: "Notification title",
                    body: "<a href='http://null.jsbin.com'>null.jsbin.com</a>",
                    age: "5 min ago",
                    expanded: true,
                    timeout: 0
                },
                {
                    summary: "Notification title",
                    body: "<a href='http://null.jsbin.com'>null.jsbin.com</a>",
                    age: "10 min ago",
                    expanded: true,
                    timeout: 0
                }
        ]
    }
]

var groupNoty2 = [
        {
            appTitle: "Chomium",
            appIcon: "../img/chromium.png",
            age: "5 min ago",
            notifications: [
                {
                    summary: "Notification title",
                    body: "<a href='http://null.jsbin.com'>null.jsbin.com</a>",
                    age: "5 min ago",
                    expanded: true,
                    timeout: 0
                },
                {
                    summary: "Notification title",
                    body: "<a href='http://null.jsbin.com'>null.jsbin.com</a>",
                    age: "10 min ago",
                    expanded: true,
                    timeout: 0
                }
        ]
    }
]

var groupNoty3 = [
        {
            appTitle: "KMail",
            appIcon: "../img/mail-client.svg",
            age: "5 min ago",
            notifications: [
                    {
                        summary: "jane@doe.com",
                        body: "some new mail<br/>In: Inbox",
                        age: "10 min ago",
                        expanded: true,
                        icon: "../img/woman_headphone.jpg",
                        actions: [
                            {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                            {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                        ],
                        timeout: 0
                    },
                    {
                        summary: "joe@doe.com",
                        body: "some new mail<br/>In: Inbox",
                        age: "20 min ago",
                        icon: "../img/student_cactus.jpg",
                        expanded: false,
                        actions: [
                            {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                            {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                        ],
                        timeout: 0
                    },
                    {
                        summary: "jane@doe.com",
                        body: "some new mail<br/>In: Inbox",
                        age: "30 min ago",
                        expanded: false,
                        actions: [
                            {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                            {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                        ],
                        timeout: 0,
                    }
                ]
    }
]

var groupNoty4 = [
        {
            appTitle: "KMail",
            appIcon: "../img/mail-client.svg",
            age: "5 min ago",
            expanded: true,
            notifications: [
                {
                    summary: "jane@doe.com",
                    body: "some new mail<br/>In: Inbox",
                    age: "10 min ago",
                    expanded: true,
                    icon: "../img/woman_headphone.jpg",
                    actions: [
                        {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                        {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                    ],
                    timeout: 0
                },
                {
                    summary: "joe@doe.com",
                    body: "some new mail<br/>In: Inbox",
                    age: "20 min ago",
                    icon: "../img/student_cactus.jpg",
                    expanded: false,
                    actions: [
                        {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                        {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                    ],
                    timeout: 0
                },
                {
                    summary: "jane@doe.com",
                    body: "some new mail<br/>In: Inbox",
                    age: "30 min ago",
                    expanded: false,
                    actions: [
                        {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                        {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                    ],
                    timeout: 0,
                }
            ]
    }
]

var groupNoty5 = JSON.parse(JSON.stringify(groupNoty1));
groupNoty5[0].notifications[0].expanded = false;
groupNoty5[0].notifications[1].expanded = false;

var startNotifications = [
            {
                appTitle: "Chomium",
                appIcon: "../img/chromium.png",
                age: "5 min ago",
                notifications: [
                    {
                        summary: "Notification title",
                        body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!",
                        icon: "../img/so-icon.svg",
                        age: "5 min ago",
                        timeout: 0,
                    },
                    {
                        summary: "Notification title",
                        body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!",
                        icon: "../img/so-icon.svg",
                        age: "10 min ago",
                        timeout: 0,
                    },
                    {
                        summary: "Notification title",
                        body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!",
                        icon: "../img/so-icon.svg",
                        age: "15 min ago",
                        timeout: 0,
                    }
                ]
            },
            {
                appTitle: "E-Mails",
                appIcon: "../img/mail-client.svg",
                age: "10 min ago",
                notifications: [
                    {
                        summary: "jane@doe.com",
                        body: "some new mail<br/>In: Inbox",
                        age: "10 min ago",
                        expanded: true,
                        icon: "../img/woman_headphone.jpg",
                        actions: [
                            {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                            {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                        ],
                        timeout: 0
                    },
                    {
                        summary: "joe@doe.com",
                        body: "some new mail<br/>In: Inbox",
                        age: "20 min ago",
                        icon: "../img/student_cactus.jpg",
                        expanded: false,
                        actions: [
                            {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                            {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                        ],
                        timeout: 0
                    },
                    {
                        summary: "jane@doe.com",
                        body: "some new mail<br/>In: Inbox",
                        age: "30 min ago",
                        expanded: false,
                        actions: [
                            {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                            {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                        ],
                        timeout: 0,
                    },
                    {
                        summary: "jane@doe.com",
                        body: "some new mail<br/>In: Inbox",
                        age: "30 min ago",
                        expanded: false,
                        actions: [
                            {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                            {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"},
                        ],
                        timeout: 0
                    }
                ]
            },
            {
                appTitle: "Kdenlive",
                appIcon: "../img/kdenlive.svg",
                age: "5 min ago",
                notifications: [
                    {
                        summary: "Rendering finished",
                        body: "Finished rendering in 23 min",
                        //icon: "../img/so-icon.svg",
                        age: "15 min ago",
                        expanded: false,
                        timeout: 0,
                    }
                ]
            }
        ]

var moreNotifications = [
            {
                appTitle: "Amarok",
                appIcon: "../img/amarok.svg",
                age: "now",
                notifications: [{
                     summary: "Some song",
                     body: "from some artist",
                     age: "now",
                     expanded: false,
                     timeout: 10
                }]
            },
            {
                appTitle: "Chomium",
                appIcon: "../img/chromium.png",
                age: "now",
                notifications: [{
                     summary: "Lorum ipsum",
                     body: "from some website",
                     age: "now",
                     expanded: false,
                     timeout: 0
                }]
            },
            {
                appTitle: "E-Mails",
                appIcon: "../img/mail-client.svg",
                age: "now",
                notifications: [{
                        summary: "jane@doe.com",
                        body: "some new mail<br/>In: Inbox",
                        age: "10 min ago",
                        icon: "../img/woman_headphone.jpg",
                        actions: [
                            {source: "../img/mail-reply-sender.svg", text: "Reply", icon: "mail-reply-sender"},
                            {source: "../img/mail-mark-junk.svg", text: "Delete", icon: "mail-mark-junk"}
                        ],
                        timeout: 0
                }]
            },
            {
                appTitle: "Amarok",
                appIcon: "../img/amarok.svg",
                age: "now",
                notifications: [{
                     summary: "Some song",
                     body: "from some artist",
                     age: "now",
                     expanded: false,
                     timeout: 10
                }]
            },
            {
                appTitle: "Amarok",
                appIcon: "../img/amarok.svg",
                age: "now",
                notifications: [{
                     summary: "Some song",
                     body: "from some artist",
                     age: "now",
                     expanded: false,
                     timeout: 10
                }]
            },
            {
                appTitle: "Chomium",
                appIcon: "../img/chromium.png",
                age: "now",
                notifications: [{
                     summary: "Lorum ipsum",
                     body: "from some website",
                     age: "now",
                     expanded: false,
                     timeout: 0
                }]
            },
            {
                appTitle: "Chomium",
                appIcon: "../img/chromium.png",
                age: "now",
                notifications: [{
                     summary: "Lorum ipsum",
                     body: "from some website",
                     age: "now",
                     expanded: false,
                     timeout: 0
                }]
            },

];
