//import related modules
import QtQuick 2.7
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0
import org.kde.plasma.components 2.0 as PlasmaComponents

import "notification.js" as N
import "HIG.js" as H


//window containing the application
Item {
    id: root
    property var nData
    property ListModel notifications
    property bool popup: false;
    property bool expanded: false;
    property bool grouped: false;
    signal deleteNotification(int idxnot)

    //height: root.expanded && root.actions && root.actions.count > 0 ? childrenRect.height + 48 : childrenRect.height;
    height: N.nHeight()
    //height: 100
    width: parent.width;

    // used only for annotations
    Item {
        id: content
        visible: false
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: cmds.visible ? cmds.top : parent.bottom
    }

    Column {
        id: textCont
        y: root.grouped? units.smallSpacing * 2 : 0
        x: units.smallSpacing * 2;
        spacing: units.smallSpacing
        visible: root.popup || "expanded" in nData && nData.expanded


        Label {
            text: nData.summary
            id: summary
            //font.pixelSize: 14
            color: "#31363b"
            renderType: Text.QtRendering


            MouseArea {
                anchors.fill: parent
                onClicked: {
                    root.deleteNotification(index)
                    //root.notifications.remove(root.index)
                }
            }
        }

        Label {
            id: body
            text: nData.body
            font.pixelSize: 12
            color: "#7f8c8d"
            maximumLineCount: 3 //nData.expanded ? 3 : 1
            elide: Text.ElideRight
            renderType: Text.QtRendering

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    root.deleteNotification(index)
                    //root.notifications.remove(root.index)
                }
            }
        }
    }

    Label {
        y: root.grouped? units.smallSpacing * 2 : 0
        //height: 10
        id: abstr
        renderType: Text.QtRendering

        visible: !textCont.visible
        anchors.rightMargin: root.grouped ? units.smallSpacing * 2 : units.smallSpacing * 6
        anchors.right: root.grouped ? age.left : parent.right
        anchors.left: parent.left
        anchors.leftMargin: units.smallSpacing * 2

        text: nData.summary + " <font color='#7f8c8d'>" + nData.body + "</font>"
        //font.pixelSize: 12
        //font.pixelSize: 14
        color: "#31363b"
        elide: Text.ElideRight
        maximumLineCount: 1
        //width: parent.width
        wrapMode: Text.WrapAnywhere

        /*LinearGradient {
            anchors.fill: parent
            start: Qt.point(0, 0)
            end: Qt.point(0, 20)
            gradient: Gradient {
                GradientStop { position: 0.0; color: "#00ffffff" }
                GradientStop { position: 1.0; color: "#ccffffff" }
            }
        }*/
        MouseArea {
            anchors.fill: parent
            onClicked: {
                root.deleteNotification(index)
                //root.notifications.remove(root.index)
            }
        }
    }
    Binding {
        target: abstr; property: "visible"; value: !textCont.visible
    }

    /*Label {
        //y: root.grouped? 8 : 0
        id: dots
        text: "..."
        font.pixelSize: 10
        color: "#31363b"
        visible: abstr.visible
        font.bold: true
        anchors.left: parent.left
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: abstr.bottom
        anchors.leftMargin: 8
    }*/


    // notification Icon
    Image {
        id: notificationIcon
        visible: root.popup || "expanded" in nData && nData.expanded
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;
        anchors.rightMargin: units.smallSpacing * 2;
        anchors.bottomMargin: cmds.visible ? cmds.height + units.smallSpacing * 2 :  units.smallSpacing * 2 //"expanded" in root.nData && root.nData.expanded ? 48 : 8
        height: units.iconSizes.large;
        width: units.iconSizes.large;
        source: root.nData.icon !== undefined ? root.nData.icon : ""
        smooth: true
        z: 4
    }

    /*Image {
        visible: !( "expanded" in root.nData && root.nData.expanded && "icon" in root.nData)
        anchors.right: parent.right;
        anchors.bottom: parent.bottom;
        anchors.rightMargin: 32;
        anchors.bottomMargin: 8 //"expanded" in root.nData && root.nData.expanded ? 48 : 8
        height: 32;
        width: 32;
        source: root.nData.icon !== undefined ? root.nData.icon : ""
        smooth: true
        z: 4
    }*/

    // Expand Icon
   Image {
       id: expandIcon
        visible: abstr.visible
        height: units.iconSizes.small;
        width: units.iconSizes.small;
        source: "../img/go-down.svg"
        smooth: true
        anchors.right: closeIcon.visible ? closeIcon.left : parent.right
        anchors.verticalCenter: abstr.verticalCenter
        //anchors.top: parent.top
        anchors.rightMargin: units.smallSpacing * 2
        MouseArea {
            anchors.fill: parent
            onClicked: {
               root.nData["expanded"] = true
            }
        }
    }

    Image {
        id: unexpandIcon
        visible: !root.popup && "expanded" in root.nData && root.nData.expanded
        height: units.iconSizes.small;
        width: units.iconSizes.small;
        source: "../img/go-up.svg"
        smooth: true
        anchors.right: closeIcon.visible ? closeIcon.left : parent.right
        anchors.verticalCenter: abstr.verticalCenter
        //anchors.top: parent.top
        anchors.rightMargin: units.smallSpacing * 2
        z: 4
        MouseArea {
            anchors.fill: parent
            onClicked: {
               root.nData["expanded"] = false
            }
        }
    }

    // Close Icon
    Image {
        id: closeIcon
        visible: parent.grouped
        height: units.iconSizes.smallMedium
        width: units.iconSizes.smallMedium;
        source: "../img/im-ban-kick-user.svg"
        smooth: true
        anchors.right: parent.right
        anchors.top: !abstr.visible ? parent.top : undefined
        anchors.verticalCenter: abstr.visible ? abstr.verticalCenter : undefined
        anchors.margins: units.smallSpacing * 2

        MouseArea {
            anchors.fill: parent
            onClicked: {
                root.deleteNotification(index)
                //root.notifications.remove(root.index)
            }
        }
    }

    // Show age if grouped
    Label {
        id: age
        visible: parent.grouped
        color: "#7f8c8d"
        font.pixelSize: 12
        text: root.nData.age
        anchors.right: expandIcon.visible ? expandIcon.left : (unexpandIcon.visible ? unexpandIcon.left : closeIcon.left)
        anchors.verticalCenter: closeIcon.verticalCenter
        anchors.rightMargin: units.smallSpacing * 2
        renderType: Text.QtRendering

        MouseArea {
            anchors.fill: parent
            onClicked: {
               root.nData["expanded"] = !root.nData["expanded"]
            }
        }
    }

    // Actions for the notification
    Item {
        height: units.smallSpacing * 7
        width: parent.width;
        anchors.bottom: parent.bottom;

        id: cmds
        visible: (root.popup || ("expanded" in root.nData && root.nData.expanded)) && "actions" in root.nData && root.nData.actions !== undefined && root.nData.actions.count > 0
        //color: "#eff0f1"

        Row {
            y: units.smallSpacing
            anchors.verticalCenter: parent.verticalCenter;
            x: units.smallSpacing * 2
            Repeater {
                model: root.nData.actions
                id: actRepeater
                Item {
                    height: units.iconSizes.smallMedium
                    width: childrenRect.width + units.smallSpacing * 4
                    Row {
                        spacing: units.smallSpacing
                        /*PlasmaComponents.ToolButton {
                            iconSource: model.icon
                            text: model.text
                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                   root.notifications.remove(root.index)
                                }
                            }
                        }*/

                        Image {
                            id: actionImg
                            source: model.source
                            height: units.iconSizes.smallMedium
                            width: units.iconSizes.smallMedium
                            smooth: true
                        }
                        Label {
                            id: actionText
                            text: model.text
                            renderType: Text.QtRendering
                        }
                    }
                }
            }
        }
    }

    /*Timer {
        running: root.popup
        repeat: false
        interval: 5000
        onTriggered: root.deleteNotification(index)
    }*/

    Canvas {
        anchors.fill: root;
        id: canvas
        onPaint: {
            if (root.grouped) {
                var ctx = getContext("2d");
                ctx.strokeStyle = "rgba(189,195,199,1)";
                ctx.beginPath();
                ctx.moveTo(0, 0);
                ctx.lineTo(canvas.width, 0);
                ctx.stroke();
            }
        }
    }

    function draw(type) {
        H.draw(type);
    }


}
