import QtQuick 2.7

ListModel {
    id: model
    ListElement {
        appTitle: "Chomium"
        appIcon: "../img/chromium.png"
        age: "5 min ago"
        expanded: false
        notifications: [
            ListElement {
                summary: "Notification title"
                body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!"
                icon: "../img/so-icon.svg";
                age: "5 min ago"
                expanded: false
                timeout: 0
                actions: [
                    ListElement {source: "../img/mail-reply-sender.svg"; text: "Reply"; icon: "mail-reply-sender"},
                    ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"; icon: "mail-mark-junk"}
                ]
            },
            ListElement {
                summary: "Notification title"
                body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!"
                icon: "../img/so-icon.svg";
                age: "10 min ago"
                expanded: false
                timeout: 0
            },
            ListElement {
                summary: "Notification title"
                body: "<a href='http://null.jsbin.com'>null.jsbin.com</a><br/>Hey there!<br/>You have been notified!"
                icon: "../img/so-icon.svg";
                age: "15 min ago"
                expanded: false
                timeout: 0
            }
        ]
    }
    ListElement {
        appTitle: "E-Mails"
        appIcon: "../img/mail-client.svg"
        age: "10 min ago"
        expanded: false
        notifications: [
            ListElement {
                summary: "jane@doe.com"
                body: "some new mail<br/>In: Inbox"
                age: "10 min ago"
                expanded: true
                icon: "../img/woman_headphone.jpg"
                actions: [
                    ListElement {source: "../img/mail-reply-sender.svg"; text: "Reply"; icon: "mail-reply-sender"},
                    ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"; icon: "mail-mark-junk"}
                ]
                timeout: 0
            },
            ListElement {
                summary: "joe@doe.com"
                body: "some new mail<br/>In: Inbox"
                age: "20 min ago"
                icon: "../img/student_cactus.jpg"
                expanded: false
                actions: [
                    ListElement {source: "../img/mail-reply-sender.svg"; text: "Reply"; icon: "mail-reply-sender"},
                    ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"; icon: "mail-mark-junk"}
                ]
                timeout: 0
            },
            ListElement {
                summary: "jane@doe.com"
                body: "some new mail<br/>In: Inbox"
                age: "30 min ago"
                expanded: false
                actions: [
                    ListElement {source: "../img/mail-reply-sender.svg"; text: "Reply"; icon: "mail-reply-sender"},
                    ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"; icon: "mail-mark-junk"}
                ]
                timeout: 0
            },
            ListElement {
                summary: "jane@doe.com"
                body: "some new mail<br/>In: Inbox"
                age: "30 min ago"
                expanded: false
                actions: [
                    ListElement {source: "../img/mail-reply-sender.svg"; text: "Reply"; icon: "mail-reply-sender"},
                    ListElement {source: "../img/mail-mark-junk.svg"; text: "Delete"; icon: "mail-mark-junk"}
                ]
                timeout: 0
            }
        ]
    }
    ListElement {
        appTitle: "Kdenlive"
        appIcon: "../img/kdenlive.svg"
        age: "5 min ago"
        expanded: false
        notifications: [
            ListElement {
                summary: "Rendering finished"
                body: "Finished rendering in 23 min"
                //icon: "../img/so-icon.svg";
                age: "15 min ago"
                expanded: false
                timeout: 0
            }
        ]
    }
    function getByName(name) {
        var i =  model.getIndexByName(name);
        if (i !== null) {
            return model.get(i);
        }
        return null;
    }

    function getIndexByName(name) {
        for (var i = 0; i < model.count; i++) {
            if (model.get(i).appTitle === name) {
                return i;
            }
        }
        return null;
    }

    function deleteNotification(idxnot, idxgrp) {
        model.get(idxgrp).notifications.remove(idxnot)
        if (model.get(idxgrp).notifications.count === 0) {
            model.remove(idxgrp)
        }
    }

    function addNotification(noty) {
        console.log(noty)
        var i =  model.getIndexByName(noty.appTitle);
        if (i !== null) {
            model.get(i).notifications.insert(0, noty.notifications[0]);
            model.move(i, 0, 1);
        }
        else {
            model.insert(0, noty)
        }
    }

    /*function appendGroup(group) {
        model.append(group);
        var grp = model.get(model.count - 1)
        grp.notifications.onCountChanged.connect(function() {
            //console.log(JSON.stringify(model))
            console.log(JSON.stringify(grp));

            if (grp.notifications.count === 0) {
                var i = model.getIndexByName(grp.appTitle);
                if (i !== null) {
                    console.log("remove group: " + i)
                    model.remove(i)
                }
            }
            //model.sync()
        });
    }*/
}
