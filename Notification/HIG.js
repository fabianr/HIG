function draw(type) {
    // Add ruler
    var ruler = Qt.createComponent("../Ruler.qml");
    var brace = Qt.createComponent("../Brace.qml");
    var outline = Qt.createComponent("../Outline.qml");
    var messure = Qt.createComponent("../Messure.qml");
    //console.log(type);
    switch(type) {
        case "structur-group":
            outline.createObject(root, {item: header, label: false});
        break;
        case "gstructur-group":
            outline.createObject(root, {item: header, label: false});
            outline.createObject(root, {item: body.itemAt(0), label: false});
            outline.createObject(root, {item: body.itemAt(1), label: false});
        break;
        case "structur-body":
            outline.createObject(root, {item: content, label: false});
            outline.createObject(root, {item: cmds, label: false});
        break;
        case "structur-body2":
            outline.createObject(root, {item: content, label: false});
        break;
        case "group-header-1":
            outline.createObject(root, {item: header, label: false});
            outline.createObject(root, {item: appIcon, label: false});
            outline.createObject(root, {item: appTitle, label: false});
            outline.createObject(root, {item: lblAge, label: false});
            outline.createObject(root, {item: imgClose, label: false});

            ruler.createObject(root, {ry: appIcon.mapToItem(root, 0, 0).y + appIcon.height / 2});
            ruler.createObject(root, {ry: appIcon.mapToItem(root, 0, 0).y + appIcon.height});
            ruler.createObject(root, {rx: appIcon.mapToItem(root, 0, 0).x,  horizontal: false});

            //brace.createObject(root, {"from": appIcon, "to": appTitle, "text": "8"});
        break;
        case "ggroup-header-1":
            outline.createObject(root, {item: header, label: false});
            outline.createObject(root, {item: appIcon, label: false});
            outline.createObject(root, {item: appTitle, label: false});
            outline.createObject(root, {item: more, label: false});
            outline.createObject(root, {item: expandIcon, label: false});
            outline.createObject(root, {item: imgClose, label: false});

            ruler.createObject(root, {ry: appIcon.mapToItem(root, 0, 0).y + appIcon.height / 2});
            ruler.createObject(root, {ry: appIcon.mapToItem(root, 0, 0).y + appIcon.height});
            ruler.createObject(root, {rx: appIcon.mapToItem(root, 0, 0).x,  horizontal: false});

            //brace.createObject(root, {"from": appIcon, "to": appTitle, "text": "8"});
        break;
        case "body-align":
            ruler.createObject(root, {rx: appIcon.mapToItem(root, 0, 0).x,  horizontal: false});
            ruler.createObject(root, {rx: imgClose.mapToItem(root, 0, 0).x + imgClose.width,  horizontal: false});
        break;
        case "gbody-align":
            ruler.createObject(root, {rx: appIcon.mapToItem(root, 0, 0).x,  horizontal: false});
            ruler.createObject(root, {rx: imgClose.mapToItem(root, 0, 0).x + imgClose.width,  horizontal: false});
            ruler.createObject(root, {ry: closeIcon.mapToItem(root, 0, 0).y});
            outline.createObject(root, {item: cmds, label: false});
            ruler.createObject(root, {ry: closeIcon.mapToItem(root, 0, 0).y + closeIcon.height / 2});
        break;
        case "body-text":
            //ruler.createObject(root, {rx: textContent.mapToItem(root, 0, 0).x,  horizontal: false});
            outline.createObject(root, {item: content, label: false});
            outline.createObject(root, {item: summary, label: false});
            outline.createObject(root, {item: body, label: false});
            outline.createObject(root, {item: notificationIcon, label: false});
            ruler.createObject(root, {ry: body.mapToItem(root, 0, 0).y + body.height});
            //brace.createObject(root, {"from": notificationIcon, "to": cmds, "text": "8", "center": false, horizontal: false});
        break;
        case "gbody-text":
            //ruler.createObject(root, {rx: textContent.mapToItem(root, 0, 0).x,  horizontal: false});
            outline.createObject(root, {item: content, label: false});
            outline.createObject(root, {item: summary, label: false});
            outline.createObject(root, {item: body, label: false});
            outline.createObject(root, {item: notificationIcon, label: false});
            outline.createObject(root, {item: closeIcon, label: false});
            outline.createObject(root, {item: age, label: false});
            ruler.createObject(root, {ry: notificationIcon.mapToItem(root, 0, 0).y + notificationIcon.height});
            //brace.createObject(root, {"from": notificationIcon, "to": cmds, "text": "8", "center": false, horizontal: false});
        break;
        case "action":
            //ruler.createObject(root, {rx: textContent.mapToItem(root, 0, 0).x,  horizontal: false});
            outline.createObject(root, {item: cmds, label: false});
            outline.createObject(root, {item: actRepeater.itemAt(0).children[0], label: false});
            outline.createObject(root, {item: actRepeater.itemAt(0).children[0].children[0], label: false});
            outline.createObject(root, {item: actRepeater.itemAt(0).children[0].children[1], label: false});
            outline.createObject(root, {item: actRepeater.itemAt(1).children[0]});
            //brace.createObject(root, {"from": actRepeater.itemAt(0), "to": actRepeater.itemAt(1), "text": "20", "center": true, "color": "rgba(41,128,185, 1)"});
        break;
    }

    return;
}
