//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtGraphicalEffects 1.0

import "HIG.js" as H
import "notification.js" as N

//window containing the application
Item {
    width: 640
    height: 480
    Column {
        spacing: 0
        History_Group {
            typesGrp: ["structur-group"]
            content: N.singleNoty6
            scl: 2
        }

        History_Group {
            typesGrp: ["structur-group"]
            typesBdy: ["body-align", "body-text"]
            content: N.singleNoty4
            scl: 2
        }

    }
    BaselineGrid {
        z: 1
        base: units.smallSpacing * 2
        visible: true
    }
}

