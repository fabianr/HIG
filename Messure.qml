//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

//window containing the application
Canvas {
    anchors.fill: parent;
    id: canvas
    property string color: "rgba(218, 68, 83, 0.8)"
    property Item from
    property Item to
    property string type: "left"
    property int rx;
    property int ry;
    z: 3
    property string text

    Text {
        id: label
        color: "#da4453"
        font.pointSize: 8
        lineHeight: 8
        height: 8
        text: canvas.text
    }

    onPaint: {
        var ctx = getContext("2d");
        ctx.strokeStyle = canvas.color;
        ctx.lineWidth = 1
        ctx.beginPath();

        var cFrom = from.mapToItem(canvas.parent, 0, 0);
        var cTo = to.mapToItem(canvas.parent, 0, 0);

        // Horizontal messure
        if (type == "left" || type == "right") {
            var y;
            if (canvas.ry) {
                y = ry;
            }
            else {
                if (from.height < to.height) {
                    y = cFrom.y + from.height / 2;
                }
                else {
                    y = cTo.y + to.height / 2;
                }
            }
            if (type == "right") {
                cFrom.x += from.width
                cTo.x += to.width
            }
            ctx.moveTo(cFrom.x + 4, y);
            ctx.lineTo(cTo.x - 4, y);

            // Write distance
            // TODO center it for real
            if (canvas.text == "") {
                label.text = cTo.x - cFrom.x;
            }
            label.x = cFrom.x + (cTo.x - cFrom.x) / 2  - 8
            label.y = y - 16
        }
        else {
            var x;
            if (canvas.rx) {
                x = rx;
            }
            else {
                if (from.width < to.width) {
                    x = cFrom.x + from.width / 2;
                }
                else {
                    x = cTo.x + to.width / 2;
                }

            }
            if (type == "bottom") {
                cFrom.y += from.height
                cTo.y += to.height
            }
            ctx.moveTo(x, cFrom.y + 4);
            ctx.lineTo(x, cTo.y - 4);

            // Write distance
            if (canvas.text == "") {
                label.text = cTo.y - cFrom.y;
            }
            label.y = cFrom.y + (cTo.y - cFrom.y) / 2 - 8
            label.x = x + 8
        }

        ctx.stroke();


    }
}
