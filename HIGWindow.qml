//import related modules
import QtQuick 2.7
import QtQuick.Controls 1.4
import QtGraphicalEffects 1.0

//window containing the application
Rectangle {
    id: root
    property string title;
    default property alias data : windowContent.data
    //border: "1px solid 4a545b"
    height: childrenRect.height;
    width: childrenRect.width;
    border.width: 1
    border.color: "#4a545b"

    DropShadow {

       anchors.fill: root
       horizontalOffset: 5
       verticalOffset: 5
       radius: 8.0
       samples: 17
       color: "#25666666"
       source: root
   }

    Rectangle {
        id: header
        color: "#4a545b"
        height: 28
        width: root.width

        Label {
            anchors.centerIn: parent
            color: "white"
            text: root.title;
        }

        Row {
            anchors.right: header.right
            anchors.rightMargin: 8
            height: parent.height - 4
            y: 2
            spacing: 8
            Image {
                height: parent.height - 8;
                width: parent.height - 8;
                source: "img/go-down.svg"
                smooth: true
                y: 4
                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: "#99ffffff"
                }
            }
            Image {
                height: parent.height - 8;
                width: parent.height - 8;
                y: 4
                source: "img/window-maximize-symbolic.svg"
                smooth: true
                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: "#99ffffff"
                }
            }
            Image {
                height: parent.height;
                width: parent.height;
                source: "img/window-close-symbolic.svg"
                smooth: true
            }
        }

    }
    Item {
        id: windowContent
        anchors.top: header.bottom
        anchors.left: root.left
        height: childrenRect.height;
        width: childrenRect.width;
    }

}
