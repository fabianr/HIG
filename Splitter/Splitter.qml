//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 320
    height: 320

    GroupBox {
        style: Styles.GroupBoxStyle {}
        anchors.fill: parent
        anchors.margins: units.smallSpacing * 2
        title: "Settings"
        z: 2
        flat: true

        SplitView {
            orientation: Qt.Horizontal
            anchors.fill: parent

            ExclusiveGroup {
                id: radioInputGroup
            }
            Column {
                Layout.minimumWidth: 120
                spacing: 0
                RadioButton {
                    exclusiveGroup: radioInputGroup
                    text: "Blue"
                }
                RadioButton {
                    exclusiveGroup: radioInputGroup
                    text: "Green"
                    checked: true
                }
                RadioButton {
                    exclusiveGroup: radioInputGroup
                    text: "Red"
                }
                RadioButton {
                    exclusiveGroup: radioInputGroup
                    text: "Yellow"
                }
            }

            ExclusiveGroup {
                id: radioInputGroup2
            }
            Column {
                Layout.minimumWidth: 120
                spacing: 0
                RadioButton {
                    exclusiveGroup: radioInputGroup2
                    text: "Circle"
                }
                RadioButton {
                    exclusiveGroup: radioInputGroup2
                    text: "Pentagon"
                    checked: true
                }
                RadioButton {
                    exclusiveGroup: radioInputGroup2
                    text: "Rectangle"
                }
                RadioButton {
                    exclusiveGroup: radioInputGroup2
                    text: "Triangle"
                }
            }

        }
    }
}
