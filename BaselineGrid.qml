//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

//window containing the application
Item {
    anchors.fill: parent;
    property int base: units.gridUnit
    property string color: "rgba(200, 200, 200, 0.7)"
    property string label: "units.gridUnit / 18px"
    property bool touch: false;
    property bool mouse: false;


    Canvas {
        anchors.fill: parent;
        id: canvas
        onPaint: {
            var ctx = getContext("2d");
            ctx.strokeStyle = color;
            ctx.beginPath();

            // Horizontal grid lines
            console.log("drawing a " + base + "x" +  base + " grid. units.smallSpacing: " + units.smallSpacing);
            var i = 0;
            while (i < canvas.height / base) {
                ctx.moveTo(0, i * base);
                ctx.lineTo(canvas.width, i * base);
                i++;
            }

            // Vertical grid lines
            i = 0;
            while (i < canvas.width / base) {
                ctx.moveTo(i * base, 0);
                ctx.lineTo(i * base, canvas.height);
                i++;
            }
            ctx.stroke();

            // draw scale
            if (label != "") {
                ctx.strokeStyle = "#da4453";
                ctx.beginPath();
                var left = canvas.width - 2 * base - canvas.width % base;
                var top;
                if (canvas.height % base  > 2 * units.smallSpacing) {
                    top = canvas.height - canvas.height % base;
                }
                else {
                    top = canvas.height - base - canvas.height % base;
                }

                ctx.moveTo(left, top - units.smallSpacing);
                ctx.lineTo(left, top + units.smallSpacing);

                ctx.moveTo(left, top);
                ctx.lineTo(left + base, top);

                ctx.moveTo(left + base, top - units.smallSpacing);
                ctx.lineTo(left + base, top + units.smallSpacing);
                ctx.stroke();
            }


         }
    }

    Label {
        id: lbl
        text: label
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.bottomMargin: canvas.height % base  > 2 * units.smallSpacing ? canvas.height % base + units.smallSpacing : base + canvas.height % base + units.smallSpacing
        anchors.rightMargin: base
        color: "#da4453"
        font.pixelSize: 10
        renderType: Text.QtRendering
    }
    Row {

        anchors.bottom: lbl.top
        anchors.right: lbl.right
        anchors.margins: units.smallSpacing
        spacing: units.smallSpacing
        Image {
            visible: mouse
            height: units.iconSizes.smallMedium;
            width: units.iconSizes.smallMedium;
            source: "img/edit-select.svg"
            smooth: true
        }
        Image {
            visible: touch
            height: units.iconSizes.smallMedium;
            width: units.iconSizes.smallMedium;
            source: "img/transform-browse.svg"
            smooth: true
        }
    }
}
