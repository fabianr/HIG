//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore

Rectangle {
    width: 420
    height: 300
    id: root

    property int lsize: 240;
    property int fsize: 120

    GridLayout {
        x: 20
        y: 20
        columns: 2
        id: group1

        Label {
            id: label1
            text: "Caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: lsize
        }
        TextField {
            Layout.minimumWidth: fsize
        }
        Label {
            text: "One more long caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: lsize
        }
        TextField {
            Layout.minimumWidth: fsize
        }
        Label {
            text: "Very Very Very Very long caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: lsize
        }
        TextField {
            Layout.minimumWidth: fsize
        }


        Label {
            Layout.topMargin: 16
            text: "Caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: lsize
        }
        ComboBox {
            Layout.topMargin: 16
            model: ["alpha"]
            Layout.minimumWidth: fsize
        }
        Label {
            text: "One more:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: lsize
        }
        SpinBox {
            value: 42
            Layout.minimumWidth: fsize
        }
        Label {
            text: "Long caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: lsize
        }
        CheckBox {
            text: "Check me"
        }
    }

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: 4
        color: "rgba(200, 200, 200, 0.2)"
    }
}
