//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore

Rectangle {
    width: 420
    height: 300
    id: root

    GridLayout {
        x: 20
        y: 20
        columns: 2
        id: group1

        Label {
            id: label1
            text: "Caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        TextField {
            Layout.minimumWidth: 400 - 120 - 2 * 20
        }
        Label {
            text: "Long caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        TextField {
            Layout.minimumWidth: 400 - 120 - 2 * 20
        }
        Label {
            text: "Very long caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        TextField {
            Layout.minimumWidth: 400 - 120 - 2 * 20
        }
    }

    GridLayout {
        anchors.top: group1.bottom
        anchors.topMargin: 16
        y: 20
        columns: 2

        Label {
            text: "Caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        ComboBox {
            model: ["alpha"]
            Layout.minimumWidth: 120
        }
        Label {
            text: "One more:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        SpinBox {
            value: 42
            Layout.minimumWidth: 120
        }
        Label {
            text: "Long caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        CheckBox {
            text: "Check me"
        }
    }

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: 4
        color: "rgba(200, 200, 200, 0.2)"
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            var ruler = Qt.createComponent("../Ruler.qml");
            var brace = Qt.createComponent("../Brace.qml");
            var outline = Qt.createComponent("../Outline.qml");
            var messure = Qt.createComponent("../Messure.qml");
            ruler.createObject(root, {rx: label1.mapToItem(root, 0, 0).x, horizontal: false});
            return;
        }
    }
}
