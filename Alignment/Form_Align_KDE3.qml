//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 400
    height: 160

    GridLayout {
        x: 20
        y: 20
        columns: 2
        Label {
            text: "Profile name:"
        }
        TextField {

        }
        Label {
            text: "Command:"
        }
        TextField {
        }
        Label {
            text: "Initial directory:"
        }
        TextField {
        }
    }
}
