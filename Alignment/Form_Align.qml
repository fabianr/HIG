//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.core 2.0 as PlasmaCore
import "Form_Align-1.js" as Mark

Rectangle {
    width: 420
    height: 300
    id: root

    GridLayout {
        x: 20
        y: 20
        columns: 2
        id: group1

        Label {
            id: label1
            text: "Caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        TextField {
            id: input1
            Layout.minimumWidth: 400 - 120 - 2 * 20
        }
        Label {
            text: "Long caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        TextField {
            Layout.minimumWidth: 400 - 120 - 2 * 20
        }
        Label {
            id: label3
            text: "Very long caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        TextField {
            id: input3
            Layout.minimumWidth: 400 - 120 - 2 * 20
        }


        Label {
            id: label4
            Layout.topMargin: 16
            text: "Caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        ComboBox {
            id: input4
            Layout.topMargin: 16
            model: ["alpha"]
            Layout.minimumWidth: 120
        }
        Label {
            text: "One more:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        SpinBox {
            value: 42
            Layout.minimumWidth: 120
        }
        Label {
            text: "Long caption:"
            horizontalAlignment: Text.AlignRight
            Layout.minimumWidth: 120
        }
        CheckBox {
            text: "Check me"
        }
    }

    function draw(type) {
        Mark.draw(type);
    }
}
