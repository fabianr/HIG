for f in *.qml;
do
	qml $f & 
	sleep 2
	window=$(wmctrl -l | grep "Qml" | awk '{print $1}')
	output=${f:-4}.png
	import -window $window $output
	pkill qml;
done;

