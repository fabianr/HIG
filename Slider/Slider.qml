//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
// import org.kde.plasma.core 2.0 as PlasmaCore
// import org.kde.plasma.components 2.0 as PlasmaComponents
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 480
    height: 128
    id: root

    GridLayout {
        columns: 3
        columnSpacing: units.smallSpacing * 2
		x: 20
		y: 32
		width: 280
		
		Label {
			id: lbl1
			width: 60
			Layout.alignment: Qt.AlignRight
			anchors.top: parent.top
			//anchors.top: parent.top
			text: "Select screen size:"
		}
		
		ColumnLayout {
            spacing: units.smallSpacing * 2
			Item {
				width: 200
                height: units.largeSpacing
				Slider {
					id: sld1
					width: 200
					value: 1
					maximumValue: 5
					minimumValue: 0
					tickmarksEnabled: true
					style: Styles.SliderStyle {
						tickmarks: Repeater {
							id: repeater
							model: [0,1,2,3,4,5]
							Rectangle {
								color: "#777"
								width: 1 ; 
								height: index == 0 || index == 5 ? 8 : 4
								y: repeater.height
								x:  styleData.handleWidth / 2 + index * ((repeater.width - styleData.handleWidth) / (repeater.count-1))
							}
						}
					}
				}
			}
			Item {
				width: 200
                height: units.largeSpacing
				Label {
					text: "640x480"
				}
				Label {
					text: "5120×2880"
					anchors.right: parent.right
				}
			}
		}
		
		Label {
			id: lbl2
			text: "1024x768"
			anchors.top: parent.top
            height: units.largeSpacing
		}
	}
	
	BaselineGrid {
		z: 1
		color: "rgba(150, 150, 150, 0.2)"
	}
	
	// HACK coordinates are only final after a small delay
	Timer {
		interval: 1000
		repeat: false
		running: true
		onTriggered: {
			//Add ruler
			var ruler = Qt.createComponent("../Ruler.qml");
			ruler.createObject(root, {ry: lbl1.mapToItem(root, 0, 0).y + lbl1.height});
			ruler.createObject(root, {rx: sld1.mapToItem(root, 0, 0).x, horizontal: false});
			ruler.createObject(root, {rx: sld1.mapToItem(root, 0, 0).x + sld1.width, horizontal: false});
// 
			var brace = Qt.createComponent("../Brace.qml");
            brace.createObject(root, {"from": lbl1, "to": sld1, "text": "8px", center: false});
            brace.createObject(root, {"from": sld1, "to": lbl2, "text": "8px", center: false});

			var outline = Qt.createComponent("../Outline.qml");
			outline.createObject(root, {item: sld1, label: false});
			outline.createObject(root, {item: lbl1, label: false});
			outline.createObject(root, {item: lbl2, label: false});
        }
    }
}
