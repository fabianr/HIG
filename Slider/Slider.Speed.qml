//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 480
    height: 120
    id: root

    GridLayout {
        columns: 3
        columnSpacing: units.smallSpacing * 2
		x: 20
		y: 32
		width: 280
		
		Label {
			id: lbl1
			width: 60
			Layout.alignment: Qt.AlignRight
            anchors.verticalCenter: lbl2
			//anchors.top: parent.top
            text: "Movement speed"
		}
		
		ColumnLayout {
            spacing: 8
			Item {
				width: 200
                height: units.largeSpacing
				Slider {
					id: sld1
					width: 200
					value: 1
					maximumValue: 5
					minimumValue: 0
					tickmarksEnabled: true
				}
			}
		}
        TextField {
            id: lbl2
            Layout.maximumWidth: 60
            text: "1.5"
            anchors.top: parent.top
            height: 12
		}
	}
	
	BaselineGrid {
		z: 1
		color: "rgba(150, 150, 150, 0.2)"
	}
	
	// HACK coordinates are only final after a small delay
	Timer {
		interval: 1000
		repeat: false
		running: true
		onTriggered: {
			//Add ruler
			var ruler = Qt.createComponent("../Ruler.qml");
			ruler.createObject(root, {ry: lbl1.mapToItem(root, 0, 0).y + lbl1.height});
// 
			var brace = Qt.createComponent("../Brace.qml");
			brace.createObject(root, {"from": lbl1, "to": sld1, "text": "8", center: false});
			brace.createObject(root, {"from": sld1, "to": lbl2, "text": "8", center: false});

			var outline = Qt.createComponent("../Outline.qml");
        }
    }
}
