//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 480
    height: 160
    id: root

    GridLayout {
        columns: 3
        columnSpacing: units.smallSpacing * 2
        rowSpacing: 72
		x: 20
		y: 32
		width: 280
		
		Label {
			id: lbl1
			width: 60
			Layout.alignment: Qt.AlignRight
			anchors.top: parent.top
			//anchors.top: parent.top
            text: "Icon <u>s</u>ize:"
		}
		
		ColumnLayout {
            spacing: units.smallSpacing * 2
			Item {
				width: 200
                height: units.largeSpacing
				Slider {
					id: sld1
					width: 200
					value: 1
					maximumValue: 5
					minimumValue: 0
				}
			}
		}
		
		Label {
			id: lbl2
			text: "112px"
			anchors.top: parent.top
            height: units.largeSpacing
		}
		Label {
			x: 5
			y: 5
		}
	
		Item {
			width: 200
            height: units.largeSpacing
			Slider {
				id: sld2
				width: 200
				value: 1
				maximumValue: 5
				minimumValue: 0
			}
			Rectangle {
				color: "#3b4045"
                height: childrenRect.height + units.smallSpacing;
                width: childrenRect.width + units.smallSpacing * 2;
				x: 20
				y: -30
				Label {
                    x: units.smallSpacing
                    y: units.smallSpacing * 1.5
					color: "#fcfcfc"
					id: lbl3
					text: "112px"
					anchors.top: parent.top
                    height: units.largeSpacing
				}
			}
		}
		Rectangle {
			x: 5
			y: 5
		}
	}
	
	BaselineGrid {
		z: 1
		color: "rgba(150, 150, 150, 0.2)"
	}
	
	// HACK coordinates are only final after a small delay
	Timer {
		interval: 1000
		repeat: false
		running: true
		onTriggered: {
			//Add ruler
        }
    }
}
