//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 640
    height: 120
    id: root

    GridLayout {
        columns: 4
        columnSpacing: 8
		x: 20
		y: 30
		width: 280
		
		Label {
			id: lbl1
			width: 60
			Layout.alignment: Qt.AlignRight
			anchors.top: parent.top
			//anchors.top: parent.top
			text: "Select screen size:"
		}
		Rectangle {
			width: 20
		}
		
		Label {
			id: lbl2
			text: "1024x768"
			height: 20
		}
		Rectangle {
			width: 20
		}
		
		Rectangle {
			width: 20
		}
		Label {
			id: lbl3
			anchors.topMargin: 16
			text: "640x480"
		}
		
		Item {
			width: 200
			height: 20
			Slider {
				id: sld1
				width: 200
				value: 1
				maximumValue: 5
				minimumValue: 0
				tickmarksEnabled: true
				style: Styles.SliderStyle {
					tickmarks: Repeater {
						id: repeater
						model: [0,1,2,3,4,5]
						Rectangle {
							color: "#777"
							width: 1 ; height: 3
							y: repeater.height
							x:  styleData.handleWidth / 2 + index * ((repeater.width - styleData.handleWidth) / (repeater.count-1))
						}
					}
				}
			}
		}
		Label {
			id: lbl4
			text: "UHD"
		}
	}
	
	BaselineGrid {
        z: 1
        base: 4
        color: "rgba(150, 150, 150, 0.2)"
    }
    
    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            //Add ruler
            var ruler = Qt.createComponent("../Ruler.qml");
            ruler.createObject(root, {ry: lbl1.mapToItem(root, 0, 0).y + lbl1.height - 4});
// 
            var brace = Qt.createComponent("../Brace.qml");
            brace.createObject(root, {"from": lbl1, "to": lbl3, "text": "8", center: false});

//             var outline = Qt.createComponent("../Outline.qml");
//             outline.createObject(root, {item: sld1, label: false});
//             outline.createObject(root, {item: lbl1, label: false});
        }
    }
}
