//import related modules
import QtQuick 2.7
//import QtQuick.Layouts 1.3
//import QtQuick.Controls 1.4
//import QtQuick.Window 2.2
//import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.components 2.0 as PlasmaComponents

//window containing the application
Rectangle {
    width: 640
    height: 480
    scale: 1
    smooth: true

    property int spacing: 10

    PlasmaComponents.Label {
        text: "Label:"
        x: parent.spacing;
        y: parent.spacing;
    }
    PlasmaComponents.ComboBox {
        anchors.left: parent.left
        anchors.right: parent.right
        model: [ "Items" ]
        x: parent.spacing;
        y: 6 * parent.spacing;
    }

    PlasmaComponents.TextField {
        x: parent.spacing;
        y: 11 * parent.spacing;
    }

    PlasmaComponents.CheckBox {
        //Layout.topMargin: 100
        text: "Checkbox"
        x: parent.spacing;
        y: 16 * parent.spacing;
    }

    /*BaselineGrid {
        step: 12
    }*/
}
