function draw(type) {
    var outline = Qt.createComponent("../Outline.qml");
    var brace = Qt.createComponent("../Brace.qml");
    var ruler = Qt.createComponent("../Ruler.qml");

    switch(type) {
        case "padding":
            // Defne and draw braces
            var braces = [
                {"from": comboBox1Label, "to": comboBox1, "text": "4", horizontal: false},
                {"from": comboBox2, "to": checkbox1, "text": "18", horizontal: false},
                //{"from": checkbox2, "to": checkbox1, "text": "4", horizontal: false},
                {"from": checkbox3, "to": checkbox2, "text": "4", horizontal: false},
                {"from": col1col1, "to": col1col2, "text": "18", "center": false, horizontal: false},
                {"from": col2, "to": col1, "text": "36", "center": false},
                {"from": textField1Label, "to": comboBox3Label, "text": "18", "center": false, horizontal: false}
            ];


            // Create a instance for each brace
            for (var i = 0; i < braces.length; i++) {
                brace.createObject(root, braces[i]);
            }

            // Add outline
            outline.createObject(root, {item: checkbox1});
            outline.createObject(root, {item: checkbox2});
            outline.createObject(root, {item: checkbox3});
            outline.createObject(root, {item: comboBox1});
            outline.createObject(root, {item: comboBox2});
            outline.createObject(root, {item: comboBox3});
            outline.createObject(root, {item: comboBox1Label});
            outline.createObject(root, {item: comboBox2Label});
            outline.createObject(root, {item: comboBox3Label});
            outline.createObject(root, {item: textField1Label});
            outline.createObject(root, {item: textField1});
        break;
        case "grid":
            // Defne and draw braces
            var braces = [
                {"from": comboBox1Label, "to": comboBox1, "text": "6", horizontal: false},
                {"from": comboBox2, "to": checkbox1, "text": "24", horizontal: false},
                //{"from": checkbox2, "to": checkbox1, "text": "4", horizontal: false},
                {"from": checkbox3, "to": checkbox2, "text": "0", horizontal: false},
                {"from": col1col1, "to": col1col2, "text": "24", "center": false, horizontal: false},
                {"from": col2, "to": col1, "text": "36", "center": false},
                {"from": textField1Label, "to": comboBox3Label, "text": "24", "center": false, horizontal: false}
            ];


            // Create a instance for each brace
            for (var i = 0; i < braces.length; i++) {
                brace.createObject(root, braces[i]);
            }

            // Add outline
            outline.createObject(root, {item: checkbox1});
            outline.createObject(root, {item: checkbox2});
            outline.createObject(root, {item: checkbox3});
            outline.createObject(root, {item: comboBox1});
            outline.createObject(root, {item: comboBox2});
            outline.createObject(root, {item: comboBox3});
            outline.createObject(root, {item: comboBox1Label});
            outline.createObject(root, {item: comboBox2Label});
            outline.createObject(root, {item: comboBox3Label});
            outline.createObject(root, {item: textField1Label});
            outline.createObject(root, {item: textField1});
        break;
        case "align":
            // Add ruler
            ruler.createObject(root, {ry: col1.mapToItem(root, 0, 0).y});
            ruler.createObject(root, {rx: col1.mapToItem(root, 0, 0).x, horizontal: false});
            ruler.createObject(root, {rx: col2.mapToItem(root, 0, 0).x, horizontal: false});
        break;

    }
}
