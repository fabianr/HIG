//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import "Spacing.js" as Logic

//window containing the application
Rectangle {
    width: 480
    height: 320
    scale: 1
    smooth: true
    id: root

    Spacing {
        id: spacing
        // Draw helpers and anotation
        BaselineGrid {
            z: 1
            base: spacing.base
        }
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            spacing.draw("align");
            return;
        }
    }
}
