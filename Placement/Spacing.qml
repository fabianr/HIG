//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
//import QtQuick.Window 2.2
//import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.core 2.0 as PlasmaCore

import "Spacing.js" as Logic

//window containing the application
Rectangle {
    width: 480
    height: 320
    scale: 1
    smooth: true
    id: root
    property int base: units.gridUnit


    Rectangle {
        z: 3
        id: box
        //title: "Group Name"
        x: base * 2
        y: base * 2
        //style: Styles.GroupBoxStyle {}
        //flat: true*/

        RowLayout {
            spacing: 2 * units.largeSpacing
            Column {
                id: col1
                Layout.preferredWidth: base * 8
                spacing: base
                Column {
                    id: col1col1
                    spacing: units.smallSpacing
                    anchors.left: parent.left
                    anchors.right: parent.right
                    PlasmaComponents.Label {
                        text: "Label:"
                        id: comboBox1Label
                    }
                    PlasmaComponents.ComboBox {
                        id: comboBox1
                        anchors.left: parent.left
                        anchors.right: parent.right
                        model: [ "Items" ]
                    }
                }
                Column {
                    id: col1col2
                    spacing: units.smallSpacing
                    anchors.left: parent.left
                    anchors.right: parent.right
                    PlasmaComponents.Label {
                        text: "Label:"
                        id: comboBox2Label
                    }
                    PlasmaComponents.ComboBox {
                        id: comboBox2
                        anchors.left: parent.left
                        anchors.right: parent.right
                        model: [ "Items" ]
                    }
                }

                Column {
                    spacing: units.smallSpacing
                    PlasmaComponents.CheckBox {
                        id: checkbox1
                        text: "Checkbox"
                        height: 28
                        // HACK bc CheckBox seems to be 26px heigh, not a multiple of the grid base
                    }
                    PlasmaComponents.CheckBox {
                        id: checkbox2
                        text: "Checkbox"
                        height: 28
                    }
                    PlasmaComponents.CheckBox {
                        id: checkbox3
                        text: "Checkbox"
                        height: 28
                    }
                }
            }
            Column {
                id: col2
                anchors.top: col1.top
                Layout.preferredWidth: 204
                spacing: units.largeSpacing
                Column {
                    spacing: units.smallSpacing
                    PlasmaComponents.Label {
                        text: "Label:"
                        id: textField1Label
                    }
                    PlasmaComponents.TextField {
                        placeholderText: "Search ..."
                        id: textField1
                    }
                }
                Column {
                    spacing: units.smallSpacing

                    PlasmaComponents.Label {
                        text: "Label:"
                        id: comboBox3Label
                    }
                    PlasmaComponents.ComboBox {
                        id: comboBox3

                        model: [ "Items" ]
                    }
                }
            }
        }
    }
    function draw(type) {
        Logic.draw(type);
    }
}
