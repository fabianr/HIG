//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
//import QtQuick.Controls 1.4
//import QtQuick.Window 2.2
//import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import org.kde.plasma.components 2.0 as PlasmaComponents
//import org.kde.plasma.core 2.0 as PlasmaCore

Rectangle {
    width: 800
    height: 200
    scale: 1
    smooth: true
    id: root

    Rectangle {
        id: box
        x: units.gridUnit * 2
        y: units.gridUnit * 2

        Grid {
            spacing: units.largeSpacing
            columns: 7

            PlasmaComponents.Label {
                text: "Label"
            }
            PlasmaComponents.Label {
                text: "ComboBox"
            }
            PlasmaComponents.Label {
                text: "Checkbox"
            }
            PlasmaComponents.Label {
                text: "TextField"
            }
            PlasmaComponents.Label {
                text: "RadioButton"
            }
            PlasmaComponents.Label {
                text: "Slider"
            }
            PlasmaComponents.Label {
                text: "Button"
            }



            PlasmaComponents.Label {
                text: "Label"
                id: lbl
            }

            PlasmaComponents.ComboBox {
                id: comboBox
                model: [ "Items" ]
            }

            PlasmaComponents.CheckBox {
                id: checkbox
                text: "Checkbox"
            }

            PlasmaComponents.TextField {
                placeholderText: "Search ..."
                id: textField
            }

            PlasmaComponents.RadioButton {
                text: "RadioButton"
                id: radioBtn
            }
            PlasmaComponents.Slider {
                id: sld
                width: 50
            }
            /*PlasmaComponents.Switch {
                id: swch
                text: "Switch"
            }*/
            PlasmaComponents.Button {
                id: btn
                text: "Button"
            }


            PlasmaComponents.Label {
                text: "Label"
            }

            PlasmaComponents.ComboBox {
                model: [ "Items" ]
            }

            PlasmaComponents.CheckBox {
                //text: "Checkbox"
            }

            PlasmaComponents.TextField {
                placeholderText: "Search ..."
            }

            PlasmaComponents.RadioButton {
                text: "RadioButton"
            }
            PlasmaComponents.Slider {
                width: 50
            }
            /*PlasmaComponents.Switch {
                id: swch
                text: "Switch"
            }*/
            PlasmaComponents.Button {
                text: "Button"
            }
        }
    }

    BaselineGrid {
        z: 1
        base: units.gridUnit
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            var outline = Qt.createComponent("../Outline.qml");
            var ruler = Qt.createComponent("../Ruler.qml");
            outline.createObject(root, {item: lbl, label: true});
            outline.createObject(root, {item: comboBox,  label: true});
            outline.createObject(root, {item: checkbox,  label: true});
            outline.createObject(root, {item: textField,  label: true});
            outline.createObject(root, {item: radioBtn,  label: true});
            outline.createObject(root, {item: sld,  label: true});
            outline.createObject(root, {item: btn,  label: true});
            ruler.createObject(root, {ry: box.mapToItem(root, 0, 0).y + 114});
            return;
        }
    }
}
