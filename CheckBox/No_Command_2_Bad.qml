//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

//window containing the application
Rectangle {
    width: 160
    height: 160

    GroupBox {
        title: "Finalization"
        anchors.centerIn: parent
        style: Styles.GroupBoxStyle {}

        Column {
            padding: 0
            spacing: units.smallSpacing
            CheckBox {
                text: "Print"
            }
            CheckBox {
                text: "Close"
            }
        }
    }
}
