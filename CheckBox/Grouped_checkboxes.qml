//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import org.kde.plasma.core 2.0 as PlasmaCore

Rectangle {
    width: 480
    height: 240
    id: root

    GridLayout {
        z: 2
        x: 4 * 10
        y: 4 * 10
        columns: 2
        columnSpacing: 4 * 2
        rowSpacing: 4 * 2

        Label {
            id: lbl1
            Layout.alignment: Qt.AlignRight
            anchors.top: parent.top
            anchors.topMargin: 4
            text: "Appearance:"
        }
        ColumnLayout {
            spacing: 4
            CheckBox {
                id: cbx1
                text: "Wrap text"
            }
            CheckBox {
                id: cbx2
                text: "Show tooltip"
            }
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Text:"
        }
        TextField {
            id: txt1
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Progress:"
        }
        ProgressBar {
            id: pgr1
            width: 100
        }
    }

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: 4
        color: "rgba(150, 150, 150, 0.2)"
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            // Add ruler
            var ruler = Qt.createComponent("../Ruler.qml");
            ruler.createObject(root, {rx: cbx1.mapToItem(root, 0, 0).x, horizontal: false});
            ruler.createObject(root, {ry: lbl1.mapToItem(root, 0, 0).y + lbl1.height - 4});


            // Defne and draw braces
            var braces = [
                {"from": lbl1, "to": cbx1, "text": "8", center: false},
                {"from": cbx1, "to": cbx2, "text": "4", horizontal: false},
                {"from": txt1, "to": pgr1, "text": "8", horizontal: false},
            ];
            var brace = Qt.createComponent("../Brace.qml");

            // Create a instance for each brace
            for (var i = 0; i < braces.length; i++) {
                brace.createObject(root, braces[i]);
            }

            var outline = Qt.createComponent("../Outline.qml");
            outline.createObject(root, {item: cbx1, label: false});
            outline.createObject(root, {item: cbx2, label: false});
            outline.createObject(root, {item: lbl1, label: false});
            outline.createObject(root, {item: pgr1, label: false});
            outline.createObject(root, {item: txt1, label: false});
        }
    }
}
