//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import org.kde.plasma.core 2.0 as PlasmaCore
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 320
    height: 240
    id: root


    GridLayout {
        columns: 2
        columnSpacing: 8
        rowSpacing: 4 * 2
        x: 4 * 10
        y: 4 * 10

        Label {
            id: lbl1
            Layout.alignment: Qt.AlignRight
            //anchors.top: parent.top
            text: "Snap to grid:"
        }
        CheckBox {
            id: cbx1
            text: "Enabled"
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Grid width:"
        }
        SpinBox {
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Grid height:"
        }
        SpinBox {
        }
    }

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: 4
        color: "rgba(150, 150, 150, 0.2)"
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            // Add ruler
            var ruler = Qt.createComponent("../Ruler.qml");
            ruler.createObject(root, {rx: cbx1.mapToItem(root, 0, 0).x, horizontal: false});
            ruler.createObject(root, {ry: lbl1.mapToItem(root, 0, 0).y + lbl1.height - 4});

            var brace = Qt.createComponent("../Brace.qml");
            brace.createObject(root, {"from": lbl1, "to": cbx1, "text": "8", center: false});

            var outline = Qt.createComponent("../Outline.qml");
            outline.createObject(root, {item: cbx1, label: false});
            outline.createObject(root, {item: lbl1, label: false});
        }
    }
}
