//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 320
    height: 160

    GroupBox {
        title: "Apply"
        x: 10
        y: 10
        style: Styles.GroupBoxStyle {}
        width: 300
        ColumnLayout {
            anchors.fill: parent
            CheckBox {
                text: "Check here if you want to<br/>apply features for all functions."
                Layout.alignment: Qt.AlignTop
            }
        }
    }
}
