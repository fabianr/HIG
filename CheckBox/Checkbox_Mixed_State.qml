//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import org.kde.plasma.core 2.0 as PlasmaCore
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 160
    height: 160

    ColumnLayout {
        z: 2
        x: units.smallSpacing * 4
        y: units.smallSpacing * 4
        CheckBox {
            text: "All"
            checkedState: Qt.PartiallyChecked
            partiallyCheckedEnabled: true
        }

        CheckBox {
            text: "Option 1"
            checked: true
            Layout.leftMargin: units.smallSpacing * 2
        }

        CheckBox {
            text: "Option 2"
            checked: false
            Layout.leftMargin: units.smallSpacing * 2
        }
        CheckBox {
            text: "Option 3"
            checked: false
            Layout.leftMargin: units.smallSpacing * 2
        }
    }
    // Draw helpers and anotation
    BaselineGrid {
        z: 1
    }
}
