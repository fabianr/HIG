//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import org.kde.plasma.core 2.0 as PlasmaCore
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Window {
    width: 480
    height: 180
    visible: true

    Item {
        anchors.fill: parent;
        id: root
        z: 2

        GridLayout {

            columns: 4
            columnSpacing: units.smallSpacing * 4
            rowSpacing: units.smallSpacing * 2
            x: units.smallSpacing * 4
            y: units.smallSpacing * 8

            Label {
                id: lbl1
                text: "Keyboard Repeat"
                Layout.alignment: Qt.AlignRight
            }
            CheckBox {
                id: cbx1
                Layout.columnSpan: 3
                checked: true
                text: "Enabled"
            }

            // next row
            Item {
                // Placeholder
                width: 1
                height: 1
            }
            Label {
                Layout.alignment: Qt.AlignRight
                text: "Delay:"
            }
            Slider {
                value: 0
                maximumValue: 200
                Layout.preferredWidth: 150
            }
            SpinBox {
                 Layout.preferredWidth: 50
            }

            // next row
            Item {
                // Placeholder
                width: 1
                height: 1
            }
            Label {
                Layout.alignment: Qt.AlignRight
                text: "Rate:"
            }
            Slider {
                value: 0
                maximumValue: 200
                Layout.preferredWidth: 150
            }
            SpinBox {
                Layout.preferredWidth: 50
            }
        }
    }
    // Draw helpers and anotation
    BaselineGrid {
        z: 1
    }


    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            // Add ruler
            var ruler = Qt.createComponent("../Ruler.qml");
            ruler.createObject(root, {rx: cbx1.mapToItem(root, 0, 0).x, horizontal: false});
            //ruler.createObject(root, {rx: col1.mapToItem(root, 0, 0).x, );*/
            // Defne and draw braces

            var braces = [
                {"from": lbl1, "to": cbx1, "text": "2", center: false},
            ];
            var brace = Qt.createComponent("../Brace.qml");

            // Create a instance for each brace
            for (var i = 0; i < braces.length; i++) {
                brace.createObject(root, braces[i]);
            }
        }
    }
}
