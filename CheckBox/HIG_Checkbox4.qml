//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

//window containing the application
Rectangle {
    width: 350
    height: 150

    RowLayout {
        x: 10
        y: 10
        spacing: 50

        GroupBox {
            title: "Finalization"
            anchors.top: parent.top
            style: Styles.GroupBoxStyle {}

            ColumnLayout {
                CheckBox {
                    text: "Print"
                }
                CheckBox {
                    text: "Close"
                }
            }
        }
        GroupBox {
            title: "Finalization"
            anchors.top: parent.top
            style: Styles.GroupBoxStyle {}

            ColumnLayout {
                Button {
                    text: "Print now"
                }
                Button {
                    text: "Close now"
                }
            }
        }
    }
}
