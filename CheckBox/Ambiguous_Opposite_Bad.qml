//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 160
    height: 160

    GroupBox {
        title: "Apply"
        anchors.centerIn: parent
        style: Styles.GroupBoxStyle {}
        RowLayout {
            CheckBox {
                text: "Landscape"
                checked: true
            }
        }
    }
}
