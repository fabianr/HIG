//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import org.kde.plasma.core 2.0 as PlasmaCore
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 320
    height: 320

    Image {
        source: "../img/video-display.svg"
        anchors.fill: parent;
        sourceSize.width: parent.width
        sourceSize.height: parent.height
    }

    Switch {
        z: 2
        x: 60
        y: 80
        checked: true
    }
    Label {
        x: 120
        y: 80
        text: "Option 1"
    }
}
