//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

Rectangle {
    width: 320
    height: 160
    id: root

    GridLayout {
        x: 20
        y: 20
        columns: 2
        columnSpacing: 8
        width: 280
        Label {
            id: label1
            Layout.alignment: Qt.AlignRight
            text: "Text:"
        }
        TextField {
            id: input1
            Layout.fillWidth: true
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Color:"
        }
        TextField {
            Layout.fillWidth: true

        }

        Rectangle {
            Layout.fillWidth: true
            height: label1.height
            Layout.columnSpan: 2

            CheckBox {
                x: label1.width - 20
                text: "Wrap text"
            }
        }
        Rectangle {
            Layout.fillWidth: true
            height: label1.height
            Layout.columnSpan: 2

            CheckBox {
                x: label1.width - 20
                text: "Show tooltips"
            }
        }
    }

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        base: 4
        color: "rgba(150, 150, 150, 0.2)"
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            var ruler = Qt.createComponent("../Ruler.qml");
            var brace = Qt.createComponent("../Brace.qml");
            var outline = Qt.createComponent("../Outline.qml");
            var messure = Qt.createComponent("../Messure.qml");
            ruler.createObject(root, {rx: label1.mapToItem(root, 0, 0).x + label1.width, horizontal: false});
            ruler.createObject(root, {rx: input1.mapToItem(root, 0, 0).x, horizontal: false});
            brace.createObject(root, {"from": label1, "to": input1, "text": "8", "center": false});
            return;
        }
    }
}
