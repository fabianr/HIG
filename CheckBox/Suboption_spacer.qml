//import related modules
import QtQuick 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles

Rectangle {
    width: 480
    height: 180

    GridLayout {
        columns: 3
        columnSpacing: 20
        anchors.centerIn: parent
        CheckBox {
            text: "Enable Keyboard Repeat"
            Layout.columnSpan: 3
            checked: true
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Delay:"
            Layout.leftMargin: 30
        }
        Slider {
            value: 0
            maximumValue: 200
            Layout.preferredWidth: 150
        }
        SpinBox {
            Layout.preferredWidth: 50
        }
        Label {
            Layout.alignment: Qt.AlignRight
            text: "Rate:"
            Layout.leftMargin: 30
        }
        Slider {
            value: 0
            maximumValue: 200
            Layout.preferredWidth: 150
        }
        SpinBox {
           Layout.preferredWidth: 50
        }

    }
}
