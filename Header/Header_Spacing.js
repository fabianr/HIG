function draw(type) {
    // Add ruler
    var ruler = Qt.createComponent("../Ruler.qml");
    var brace = Qt.createComponent("../Brace.qml");
    var outline = Qt.createComponent("../Outline.qml");
    var messure = Qt.createComponent("../Messure.qml");

    switch(type) {
        case "left":
            messure.createObject(root, {from: header, to: headerContent });
            messure.createObject(root, {from: header, to: headerContent, type: "top", rx: headerContent.mapToItem(root, 0, 0).x + 16});
            messure.createObject(root, {from: headerContent, to: header, type: "bottom", rx: headerContent.mapToItem(root, 0, 0).x + 16});
            outline.createObject(root, {item: img, label: true});
            ruler.createObject(window, {rx: img.mapToItem(window, 0, 0).x, horizontal: false});
            ruler.createObject(window, {rx: label.mapToItem(window, 0, 0).x, horizontal: false});
            ruler.createObject(window, {ry: img.mapToItem(window, 0, 0).y + img.height});
            brace.createObject(root, {"from": img, "to": label, "text": "16", "center": false});
        break;
        case "overview":
            outline.createObject(root, {item: headerContent});
            messure.createObject(root, {from: header, to: headerContent });
            messure.createObject(root, {from: headerContent, to: header, type: "right"});
            messure.createObject(root, {from: header, to: headerContent, type: "top", rx: headerContent.mapToItem(root, 0, 0).x + 16});
            messure.createObject(root, {from: headerContent, to: header, type: "bottom", rx: headerContent.mapToItem(root, 0, 0).x + 16});
            ruler.createObject(menuContainer, {ry: 2 * base});
            ruler.createObject(menuContainer, {rx: menuContainer.width - 2 * base, horizontal: false});
        break;
    }

    return;

    //ruler.createObject(headerContent, {ry: 4 * base});
    //ruler.createObject(headerContent, {ry: headerContent.height - 4 * base});
    ruler.createObject(menuContainer, {ry: 2 * base});
    ruler.createObject(menuContainer, {rx: menuContainer.width - 2 * base, horizontal: false});
    ruler.createObject(window, {rx: img.mapToItem(window, 0, 0).x, horizontal: false});
    ruler.createObject(window, {rx: headerLabel2.mapToItem(window, 0, 0).x + headerLabel2.width, horizontal: false});
    //ruler.createObject(window, {rx: rightNav.mapToItem(window, 0, 0).x, horizontal: false});
}
