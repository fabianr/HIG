//import related modules
import QtQuick 2.7

//window containing the application
Rectangle {
    width: 740
    height: 500
    id: root

    property int base: 4
    property real goldenRatioA: 0.618
    property real goldenRatioB: 0.382

    /*Image {
        anchors.fill: parent
        id: bg
        source: "../img/breeze-wallpaper/1280x1024.png"
        fillMode: Image.PreserveAspectCrop
    }*/

    HIGWindow {
        x: 20
        y: 20
        title: "System Settings"
        Header {
            source: "../img/1024x768.png"
            width: 700
            title: "Wallpaper"
            subtitle: "Breeze 5.8"
            overview: "12 of 123"
        }
    }

    HIGWindow {
        x: 20
        y: 180
        title: "Pictures"
        Header {
            source: "../img/city.jpg"
            width: 700
            title: "City at Night"
            subtitle: "Singapur"
            overview: "12 of 123"
        }
    }

    HIGWindow {
        title: "Contacts"
        x: 20
        y: 340
        Header {
            source: "../img/woman_headphone.jpg"
            width: 700
            title: "Susan"
            subtitle: "+34 123 998 112"
            overview: "87 of 123"
        }
    }
}
