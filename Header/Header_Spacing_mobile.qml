//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import QtGraphicalEffects 1.0

//window containing the application
Rectangle {
    width: 360
    height: 640
    scale: 1
    smooth: true
    id: root

    Rectangle {
        id: window
        anchors.fill: parent

        Rectangle {
            id: header
            width: parent.width
            height: units.smallSpacing * 40
            color: "red"

            Image {
                anchors.fill: parent
                id: headerBg
                source: "../img/1024x768.png"
            }
            BrightnessContrast {
                id: headerBgBrightness
                anchors.fill: headerBg
                source: headerBg
                brightness: -0.7
                contrast: 0
            }
            FastBlur {
               anchors.fill: headerBg
               source: headerBgBrightness
               radius: 64
            }

            Item {
                id: fg
                x: 24 * units.smallSpacing
                y: 0
                width: units.smallSpacing * 148
                height: units.smallSpacing * 26
                Column {
                    y: 4 * units.smallSpacing
                    x: 4 * units.smallSpacing
                    spacing: 4 * units.smallSpacing
                    Image {
                        id: img
                        source: "../img/1024x768.png"
                        height: fg.height - 8 * units.smallSpacing
                        width: fg.height - 8 * units.smallSpacing
                    }
                    Column {
                        spacing: units.smallSpacing
                        id: label
                        Label {
                            text: "Plasma"
                            color: "white"
                        }
                        Label {
                            text: "Wallpaper"
                            color: "white"
                            font.pixelSize: 10
                        }
                        Label {
                            text: "1 out of 200"
                            color: "white"
                            font.pixelSize: 10
                        }
                    }
                }
            }

            Item {
                id: menuContainer
                anchors.top: parent.top;
                anchors.right: parent.right;
                width: 120;
                height: 140;

                Rectangle {
                    color: "#eff0f1"
                    anchors.top: parent.top;
                    anchors.right: parent.right;
                    anchors.margins: 2 * units.smallSpacing
                    radius: 12
                    width: 24
                    height: 24
                    Image {
                        source: "../img/menu.svg"
                        anchors.fill: parent
                        anchors.margins: units.smallSpacing / 2
                    }
                }
            }
        }

        /* Content at the bottom */
        Rectangle {
            id: content
            anchors.top: header.bottom
            anchors.left: header.left
            width: header.width
            height: 500
            color: "#eff0f1"
        }
    }
    DropShadow {
       anchors.fill: window
       horizontalOffset: 5
       verticalOffset: 5
       radius: 8.0
       samples: 17
       color: "#80000000"
       source: window
   }



    GroupBox {
        z: 3
        flat: true
        x: units.smallSpacing * 5
        y: units.smallSpacing * 5
        style: Styles.GroupBoxStyle {}
    }

    // Draw helpers and anotation
    BaselineGrid {
        z: 1
        color: "rgba(200, 200, 200, 0.2)"
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            return;
            // Add ruler
            var ruler = Qt.createComponent("../Ruler.qml");
            ruler.createObject(fg, {ry: 4 * units.smallSpacing});
            ruler.createObject(fg, {ry: fg.height - 4 * units.smallSpacing});
            ruler.createObject(menuContainer, {ry: 2 * units.smallSpacing});
            ruler.createObject(menuContainer, {rx: menuContainer.width - 2 * units.smallSpacing, horizontal: false});
            ruler.createObject(root, {rx: img.mapToItem(root, 0, 0).x, horizontal: false});
            ruler.createObject(root, {rx: rightNav.mapToItem(root, 0, 0).x, horizontal: false});

            // Defne and draw braces
            var braces = [
                {"from": img, "to": label, "text": "2", "center": false, color: "white"},
                //{"from": fg, "to": header, "text": "2", color: "white", horizontal: false},
            ];
            var brace = Qt.createComponent("../Brace.qml");

            // Create a instance for each brace
            for (var i = 0; i < braces.length; i++) {
                brace.createObject(root, braces[i]);
            }


        }
    }
}
