//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import QtGraphicalEffects 1.0

import "Header_Spacing.js" as Mark
import "MMCQ.js" as MMCQ


Item {
    id: root
    property alias image: headerBg
    property alias source: headerBg.source
    property alias color: headerBgOverlay.color
    property alias brightness: headerBgBrightness.brightness
    property alias contrast: headerBgBrightness.contrast

    Image {
        anchors.fill: parent
        id: headerBg
        fillMode: Image.PreserveAspectCrop
    }

    ColorOverlay {
        id: headerBgOverlay
        anchors.fill: headerBg
        source: headerBg
    }
    BrightnessContrast {
        id: headerBgBrightness
        anchors.fill: headerBg
        source: headerBgOverlay
        brightness: 0
        contrast: 0
    }

    FastBlur {
        id: headerBgBlur
        anchors.fill: headerBg
        source: headerBgBrightness
        radius: 48
    }
}
