//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import QtGraphicalEffects 1.0

import "Header_Spacing.js" as Mark
import "MMCQ.js" as MMCQ


Rectangle {
    id: header
    width: parent.width
    height: base * 32
    property string source;
    property string imageSourceTemp;
    property alias hmargin: headerContent.x
    property string title;
    property string subtitle;
    property string overview;

    // TODO this works only once
    onSourceChanged: {
        if (wallpaper.source == "") {
            // first run
            wallpaper.source = header.source;
        }
        else {
            // Second run
            wallpaperTmp.source = header.source;
            removeImageAnimation.start()
            showImageAnimation.start()
        }
    }

    Wallpaper {
        id: wallpaper
        anchors.fill: parent
        opacity: 1
        NumberAnimation on opacity {
            id: removeImageAnimation
            duration: 400
            running: false
            from: 1
            to: 0
            onRunningChanged: {
                 if (!running) {

                 }
            }
        }
    }
    Wallpaper {
        id: wallpaperTmp
        anchors.fill: parent
        opacity: 0
        NumberAnimation on opacity {
            id: showImageAnimation
            from: 0
            to: 1
            duration: 400
            running: false
            onRunningChanged: {
                 if (!running) {
                    imageSourceTemp = source
                 }
            }
        }
    }

    // Container for the content of the heaer
    Item {
        id: headerContent
        x: snapToGrid(header.height * goldenRatioA * 2)
        y: snapToGrid(header.height * goldenRatioB  / 2);
        width: header.width - 2 * headerContent.x
        height: snapToGrid(header.height * goldenRatioA);

        Row {
            y: 0
            x: 0
            spacing: 4 * base
            Image {
                id: img
                source: header.source
                height: headerContent.height
                width: headerContent.height
            }
            Column {
                anchors.bottom: img.bottom
                spacing: base
                id: label
                Label {
                    text: header.title
                    font.pixelSize: 12
                    color: "white"
                }

                Label {
                    id: label2
                    text: header.subtitle
                    color: "white"
                    font.pixelSize: 10
                }
            }
        }

        Label {
            id: headerLabel2
            text: header.overview
            color: "white"
            font.pixelSize: 10
            anchors.bottom: parent.bottom
            anchors.right: parent.right
        }
    }

    Item {
        id: menuContainer
        anchors.top: parent.top;
        anchors.right: parent.right;
        width: 120;
        height: 140;

        Rectangle {
            color: "#eff0f1"
            anchors.top: parent.top;
            anchors.right: parent.right;
            anchors.margins: 2 * base
            radius: 12
            width: 24
            height: 24
            Image {
                source: "../img/menu.svg"
                anchors.fill: parent
                anchors.margins: base / 2
            }
        }
    }

    Canvas {
        id: canvasHelper;
        anchors.fill: parent;
        onPaint: {
            var color = getColor(wallpaper.image);
            color = "#60" + componentToHex(color[0]) + componentToHex(color[1]) + componentToHex(color[2]);
            wallpaper.color = color;
            var dark = detectBrightness(wallpaper.image);
            wallpaper.brightness = dark > 0 ? -0.3 : 0.1;
            canvasHelper.visible = false
        }
    }

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }

    function detectBrightness(img) {
        var canvas = canvasHelper;
        canvas.width = img.width;
        canvas.height = img.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(img,0,0);

        var imageData = ctx.getImageData(0,0,canvas.width,canvas.height);
        var data = imageData.data;
        var r,g,b, max_rgb;
        var light = 0, dark = 0;

        for(var x = 0, len = data.length; x < len; x+=4) {
            r = data[x];
            g = data[x+1];
            b = data[x+2];

            max_rgb = Math.max(Math.max(r, g), b);
            // was 128
            if (max_rgb < 32)
                dark++;
            else
                light++;
        }

        var dl_diff = ((light - dark) / (img.width * img.height));
        return dl_diff;
    }

    function getColor(img) {
        var palette = getPalette(img);
        var dominantColor = palette[0];
        return dominantColor;
    }

    function getPalette(img) {
        var colorCount = 10;
        var quality = 10;

        var canvas = canvasHelper;
        canvas.width = img.width;
        canvas.height = img.height;

        var ctx = canvas.getContext("2d");
        ctx.drawImage(img,0,0);

        var imageData = ctx.getImageData(0,0,canvas.width,canvas.height);
        var data = imageData.data;

        var r, g, b, a, offset;
        var pixelArray = [];
        for(var x = 0, len = data.length; x < len; x+=4) {
            offset = x * 4;
            r = data[offset + 0];
            g = data[offset + 1];
            b = data[offset + 2];
            a = data[offset + 3];
            // If pixel is mostly opaque and not white
            if (a >= 125) {
                if (!(r > 250 && g > 250 && b > 250)) {
                    pixelArray.push([r, g, b]);
                }
            }
        }

        // Send array to quantize function which clusters values
        // using median cut algorithm
        var cmap    = MMCQ.MMCQ.quantize(pixelArray, colorCount);
        var palette = cmap? cmap.palette() : null;
        return palette;
    }

    function draw(type) {
        Mark.draw(type);
    }

    function snapToGrid(x) {
        x = Math.round(x);
        var n = Math.round(x / base);
        return n * base;
    }



    function setNewWallpaper(src) {
        wallpaperTmp.image = src;

    }
}
