//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import QtGraphicalEffects 1.0

//window containing the application
Rectangle {
    width: 740
    height: 180
    id: root

    property int base: 4
    property real goldenRatioA: 0.618
    property real goldenRatioB: 0.382

    HIGWindow {
        title: "Contacts"
        x: 20
        y: 20
        Header {
            id: header
            source: "../img/woman_headphone.jpg"
            width: 700
            title: "Susan"
            subtitle: "+34 123 998 112"
            overview: "87 of 123"
        }
    }

    /*HIGWindow {
        title: "Contacts"
        x: 120
        y: 220
        Header {
            source: "../img/student_cactus.jpg"
            width: 700
            title: "Mat"
            subtitle: "+34 224 938 763"
            overview: "88 of 123"
        }
    }*/

    Timer {
        interval: 3000
        repeat: false
        running: true
        onTriggered: {
            header.source = "../img/student_cactus.jpg";
            return;
        }
    }
}
