//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import QtGraphicalEffects 1.0

//window containing the application
Rectangle {
    width: 720
    height: 480
    id: root
    Header_Spacing {
        id: header
        x: 440
        y: 140
        scale: 2
        smooth: true


        // Draw helpers and anotation
        BaselineGrid {
            z: 1
            base: header.base
            color: "rgba(200, 200, 200, 0.2)"
        }
    }

    // HACK coordinates are only final after a small delay
    Timer {
        interval: 1000
        repeat: false
        running: true
        onTriggered: {
            header.draw("left");
            return;
        }
    }
}
