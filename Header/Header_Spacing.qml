//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import QtGraphicalEffects 1.0

import "Header_Spacing.js" as Mark

//window containing the application
Rectangle {
    property int base: 4
    property real goldenRatioA: 0.618
    property real goldenRatioB: 0.382
    property string source: "../img/1024x768.png"

    width: 220 * base
    height: 280
    scale: 1
    smooth: true
    id: root

    Image {
        anchors.fill: parent
        id: bg
        source: "../img/breeze-wallpaper/1280x1024.png"
        fillMode: Image.PreserveAspectCrop
    }

    HIGWindow {
        id: window
        height: 500
        x: 10 * base
        y: 10 * base
        title: "Contacts"

        Rectangle {
            width: 200 * base
            height: 20
            color: "#4d4d4d"
            Text {
                color: "white"
                text: "Window titlebar"
            }
        }

        Header {
            id: header
            source: "../img/woman_headphone.jpg"
            title: "Susan"
            subtitle: "+34 123 998 112"
            overview: "87 of 123"
        }

        /* Content at the bottom */
        /*Rectangle {
            id: content
            anchors.top: header.bottom
            anchors.left: header.left
            width: header.width
            height: 500
            color: "#eff0f1"

            ListModel {
                id: images
                ListElement {
                    name: "Plasma"
                    checked: true
                }
                ListElement {
                    name: "Some picture"
                }
                ListElement {
                    name: "Some picture"
                }
            }


            ListView {
                id: leftNav
                anchors.top: parent.top
                width: header.hmargin
                height: 200
                model: images

                delegate: Rectangle {
                    height: 6 * base
                    width: header.hmargin
                    color: checked ? "#3daee9" : "transparent"

                    Label {
                        y: base
                        x: 2 * base
                        text: name
                        color: checked ? "white" : "black"
                    }
                }
            }
            Rectangle {
                id: mainContent
                anchors.top: parent.top;
                anchors.left: leftNav.right
                anchors.right: parent.right
                color: "white"
                height: parent.height;
                Layout.fillWidth: true;
                width: parent.width - 2 * 32 * base
            }
        }*/
    }


    GroupBox {
        z: 3
        flat: true
        x: base * 5
        y: base * 5
        style: Styles.GroupBoxStyle {}
    }

    function draw(type) {
        header.draw(type);
    }
}
