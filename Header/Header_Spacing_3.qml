//import related modules
import QtQuick 2.7
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Controls.Styles.Plasma 2.0 as Styles
import QtGraphicalEffects 1.0

//window containing the application
Rectangle {
    width: 880
    height: 240
    id: root
    Header_Spacing {
        id: header
        source: "../img/woman_headphone.jpg"
    }
}
